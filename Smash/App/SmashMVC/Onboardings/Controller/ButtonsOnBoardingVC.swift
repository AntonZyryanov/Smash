import AppTrackingTransparency
import FacebookCore
import FirebaseRemoteConfig
import Foundation
import SnapKit
import UIKit

extension OnboardingViewController {
    @objc func continueButtonPressed() {
        if self.currentPage == 0 {
            SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.firstOnBoardingPassed.rawValue)
        }
        if self.currentPage == 1 {
            SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.secondOnBoardingPassed.rawValue)
        }
        if self.currentPage == 2 {
            SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.thirdOnBoardingPassed.rawValue)
        }
        self.currentPage += 1
        self.setOnboardingPage(number: self.currentPage)
    }
}
