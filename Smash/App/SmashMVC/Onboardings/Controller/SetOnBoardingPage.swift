import AppTrackingTransparency
import FacebookCore
import FirebaseRemoteConfig
import Foundation
import SnapKit
import UIKit

extension OnboardingViewController {
    func setOnboardingPage(number: Int) {
        if number < self.datasource.count {
            self.titleLabel.text = self.datasource[number].titleText

            self.subtitleLabel.text = self.datasource[number].subtitleText

            if number == 0 {
                if #available(iOS 14, *) {
                    ATTrackingManager.requestTrackingAuthorization { status in
                        switch status {
                        case .notDetermined:
                            print("notDetermined")
                        case .restricted:
                            FacebookCore.Settings.setAdvertiserTrackingEnabled(false)
                            print("restricted")
                        case .denied:
                            FacebookCore.Settings.setAdvertiserTrackingEnabled(false)
                            print("denied")
                        case .authorized:
                            print("authorized")
                            FacebookCore.Settings.setAdvertiserTrackingEnabled(true)
                        @unknown default:
                            print("unknown")
                        }
                    }
                } else {
                    // Fallback on earlier versions
                }
            }

            if number == 1 {
                self.subtitleLabel.textColor = ThemesColors.mainColor
                self.subtitleLabel.sizeToFit()
                self.subtitleLabel.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
                self.subtitleLabel.textAlignment = .left
                self.titleLabel.textColor = ThemesColors.mainColor
                self.titleLabel.sizeToFit()
                self.titleLabel.font = UIFont.systemFont(ofSize: 34.0, weight: .bold)
                self.titleLabel.textAlignment = .left
            } else {
                self.subtitleLabel.textColor = ThemesColors.whiteColor
                self.subtitleLabel.sizeToFit()
                self.subtitleLabel.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
                self.subtitleLabel.textAlignment = .left
                self.titleLabel.textColor = ThemesColors.whiteColor
                self.titleLabel.sizeToFit()
                self.titleLabel.font = UIFont.systemFont(ofSize: 34.0, weight: .bold)
                self.titleLabel.textAlignment = .left
            }
            self.pageControl.currentPage = number
            self.imageView.image = self.datasource[number].image

        } else {
            UserDefaults.standard.setValue(true, forKey: "isOnBoardingPassed")
            UserDefaults.standard.setValue(false, forKey: "isBlackThemeChoosed")
            UserDefaults.standard.setValue(false, forKey: "isThereIsSale")
            UserDefaults.standard.setValue(false, forKey: "isThereIsUpdate")
            UserDefaults.standard.setValue("apple.com", forKey: "updateURL")
            UserDefaults.standard.setValue(false, forKey: "isTutorialPassed")
            UserDefaults.standard.setValue("0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19", forKey: "chosenEffects")
            UserDefaults.standard.setValue(
                ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29"],
                forKey: "chosenEffectsArray"
            )
            AppDelegate.shared.router.showPaywall()
        }
    }
}
