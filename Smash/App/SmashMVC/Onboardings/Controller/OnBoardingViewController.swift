import AppTrackingTransparency
import FacebookCore
import FirebaseRemoteConfig
import Foundation
import SnapKit
import UIKit

final class OnboardingViewController: UIViewController {
    let cornerLogo = UIButton()

    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "board1")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    let continueButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(continueButtonPressed), for: .touchUpInside)
        return button
    }()

    let pageControl = UIPageControl()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = ThemesColors.mainColor
        label.numberOfLines = 0
        return label
    }()

    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = ThemesColors.mainColor
        label.numberOfLines = 0
        return label
    }()

    let datasource = Onboarding.onboardingItems
    var currentPage: Int = 0

    init() {
        super.init(nibName: nil, bundle: nil)
        self.setupElements()
        self.setupInitialLayout()
        self.setOnboardingPage(number: self.currentPage)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.insertSubview(self.imageView, at: 0)
        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: view.topAnchor),
            self.imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        view.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
    }
}
