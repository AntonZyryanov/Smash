import Foundation
import UIKit

struct OnboardingItem {
    let backgroundImageName: String
    let titleText: String
    let subtitleText: String
    let image: UIImage?

    public init(backgroundImageName: String, titleText: String, subtitleText: String, imageName: String) {
        self.backgroundImageName = backgroundImageName
        self.titleText = titleText
        self.subtitleText = subtitleText
        self.image = Bundle.main.loadImage(withName: imageName)
    }
}

enum Onboarding {
    static let onboardingItems: [OnboardingItem] = [
        .init(
            backgroundImageName: "1",
            titleText: NSLocalizedString("Hello", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            subtitleText: NSLocalizedString("Welcome to Smash!", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            imageName: "onboarding1.jpg"
        ),
        .init(
            backgroundImageName: "2",
            titleText: NSLocalizedString("Time to create!", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            subtitleText: NSLocalizedString("Create unique videos with awesome sound effects", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            imageName: "onboarding2.jpg"
        ),
        .init(
            backgroundImageName: "3",
            titleText: NSLocalizedString("Share your videos", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            subtitleText: NSLocalizedString("Save and share your creative works with your friends!", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
            imageName: "onboarding3.jpg"
        ),
    ]
}
