import AppTrackingTransparency
import FacebookCore
import FirebaseRemoteConfig
import Foundation
import SnapKit
import UIKit

extension OnboardingViewController {
    func setupElements() {
        self.continueButton.setTitle(NSLocalizedString("Continue", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        self.continueButton.backgroundColor = ThemesColors.mainColor // UIColor(red: 162 / 256, green: 27 / 256, blue: 27 / 256, alpha: 1.0)
        self.continueButton.layer.masksToBounds = true
        self.continueButton.layer.cornerRadius = 8.0

        self.pageControl.numberOfPages = self.datasource.count
        self.pageControl.currentPage = self.currentPage
    }

    func setupInitialLayout() {
        self.view.addSubview(self.continueButton)
        self.view.addSubview(self.pageControl)
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.subtitleLabel)

        self.titleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(self.subtitleLabel.snp.top).offset(-24.0)
            make.left.equalTo(16.0)
            make.right.equalTo(-16.0)
        }

        self.subtitleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(self.continueButton.snp.top).offset(-24.0)
            make.left.equalTo(16.0)
            make.right.equalTo(-16.0)
        }

        self.continueButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.pageControl.snp.top).offset(-24.0)
            make.left.equalTo(16.0)
            make.right.equalTo(-16.0)
            make.height.equalTo(50.0)
        }

        self.pageControl.snp.makeConstraints { make in
            make.bottom.equalTo(-24.0)
            make.centerX.equalTo(self.view.snp.centerX)
        }
    }
}
