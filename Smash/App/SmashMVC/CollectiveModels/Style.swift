import Foundation
import UIKit

struct Style: Codable, Hashable {
    static var styles: [Style] = []

    static var chosenStyles: [Style] = []

    let id: Int
    let styleName: String
    var isSelected: Bool = false
    var isEnabled: Bool = false
    var isService: Bool = false
    var isCellMarginIsBlack: Bool = UserDefaults.standard.bool(forKey: "isBlackThemeChoosed")
    var isLocked: Bool = false
}
