import ApphudSDK
import SafariServices
import SnapKit
import StoreKit
import UIKit

extension SettingsController {
    func controllerInit() {
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            view.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            view.backgroundColor = UIColor.darkGray
        }

        view.addSubview(self.headerView)
        view.addSubview(self.backButton)
        self.headerView.addSubview(self.settingsLabel)
        //  view.addSubview(self.cornerLogo)
        self.backButton.setImage(UIImage(named: "backButton"), for: .normal)
        self.backButton.addTarget(self, action: #selector(self.goBack), for: .touchUpInside)
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.headerView.backgroundColor = .white
        } else {
            self.headerView.backgroundColor = .gray
        }
        self.settingsLabel.text = NSLocalizedString("Settings title", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: "")
        self.settingsLabel.textColor = ThemesColors.mainColor
        self.settingsLabel.font = UIFont(name: "Helvetica", size: 18)
        self.subscribeButton.addTarget(self, action: #selector(self.subscription), for: .touchUpInside)
        self.feedbackButton.addTarget(self, action: #selector(self.feedback), for: .touchUpInside)
        self.rateButton.addTarget(self, action: #selector(self.rate), for: .touchUpInside)
        self.documentationButton.addTarget(self, action: #selector(self.documentation), for: .touchUpInside)
        self.termsAndConditionsButton.addTarget(self, action: #selector(self.termsAndConditions), for: .touchUpInside)
        self.appStoreButton.addTarget(self, action: #selector(self.appStore), for: .touchUpInside)

        self.backButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(36)
            make.width.height.equalTo(30)
        }
        self.settingsLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(8.0)
        }
        self.headerView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(90)
        }
        self.createTable()
    }

    func createTable() {
        self.myTableView = UITableView(frame: view.bounds, style: .plain)
        self.myTableView.register(MenuTableViewCell.self, forCellReuseIdentifier: self.indentifire)
        self.myTableView.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)

        self.myTableView.delegate = self
        self.myTableView.dataSource = self

        self.myTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        view.addSubview(self.myTableView)
        self.setupconstraint()
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.myTableView.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            self.myTableView.backgroundColor = UIColor.darkGray
        }
    }

    func setupconstraint() {
        self.myTableView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().inset(80)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.indentifire, for: indexPath) as! MenuTableViewCell
        // subscriptions
        cell.textLabel?.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(20)
            make.top.equalToSuperview().inset(20)
        }
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.susbcriptionView.backgroundColor = .white
            } else {
                self.susbcriptionView.backgroundColor = .gray
            }

            self.susbcriptionView.layer.cornerRadius = 10
            self.smashPremium.text = NSLocalizedString("smash sub", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: "")
            self.smashPremium.textColor = ThemesColors.mainColor
            self.smashPremium.font = UIFont(name: "Helvetica", size: 16)
            self.unlockEffects.text = NSLocalizedString("Unlock all audioeffects", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: "")
            self.unlockEffects.textColor = .lightGray
            self.unlockEffects.font = UIFont(name: "Helvetica", size: 12)
            self.subscribeButton.setTitle(NSLocalizedString("Subscribe", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)
            self.subscribeButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            cell.addSubview(self.susbcriptionView)
            self.susbcriptionView.addSubview(self.smashPremium)
            self.susbcriptionView.addSubview(self.unlockEffects)
            self.susbcriptionView.addSubview(self.subscribeButton)
            self.susbcriptionView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.top.equalToSuperview().inset(45)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(80)
            }
            self.smashPremium.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.top.equalToSuperview().inset(5)
            }
            self.unlockEffects.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.top.equalTo(smashPremium).inset(20)
            }
            self.subscribeButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.bottom.equalToSuperview().inset(5)
            }
        }

        if indexPath.row == 1 {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.feedbackView.backgroundColor = .white
            } else {
                self.feedbackView.backgroundColor = .gray
            }

            self.feedbackView.layer.cornerRadius = 10
            self.feedbackButton.setTitle("smasheditorfeedback@gmail.com", for: .normal)
            self.feedbackButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            cell.addSubview(self.feedbackView)
            self.feedbackView.addSubview(self.feedbackButton)
            self.feedbackView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.top.equalToSuperview().inset(40)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(50)
            }

            self.feedbackButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.bottom.equalToSuperview().inset(5)
            }
        }
        if indexPath.row == 2 {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.appstoreView.backgroundColor = .white
            } else {
                self.appstoreView.backgroundColor = .gray
            }

            self.appstoreView.layer.cornerRadius = 10

            self.appStoreButton.setTitle(NSLocalizedString("Copy AppStore link", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)
            self.appStoreButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            cell.addSubview(self.appstoreView)
            self.appstoreView.addSubview(self.appStoreButton)
            self.appstoreView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.top.equalToSuperview().inset(40)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(50)
            }

            self.appStoreButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.bottom.equalToSuperview().inset(5)
            }
        }
        if indexPath.row == 3 {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.rateView.backgroundColor = .white
            } else {
                self.rateView.backgroundColor = .gray
            }
            self.rateButton.setTitle(NSLocalizedString("Rate application", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)
            self.rateButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            self.rateView.layer.cornerRadius = 10
            cell.addSubview(self.rateView)
            self.rateView.addSubview(self.rateButton)
            self.rateView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.top.equalToSuperview().inset(40)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(50)
            }

            self.rateButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.bottom.equalToSuperview().inset(5)
            }
        }
        if indexPath.row == 4 {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.documentsView.backgroundColor = .white
            } else {
                self.documentsView.backgroundColor = .gray
            }

            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.termsAndConditionsView.backgroundColor = .white
            } else {
                self.termsAndConditionsView.backgroundColor = .gray
            }

            self.documentationButton.setTitle(
                NSLocalizedString("Privacy policy", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
                for: .normal
            )

            self.termsAndConditionsButton.setTitle(
                NSLocalizedString("Terms and conditions", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
                for: .normal
            )
            self.documentationButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            self.termsAndConditionsButton.setTitleColor(ThemesColors.secondaryColor, for: .normal)
            self.documentsView.layer.cornerRadius = 10
            self.termsAndConditionsView.layer.cornerRadius = 10
            cell.addSubview(self.documentsView)
            cell.addSubview(self.termsAndConditionsView)
            self.documentsView.addSubview(self.documentationButton)
            self.termsAndConditionsView.addSubview(self.termsAndConditionsButton)
            self.documentsView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(50)
            }

            self.termsAndConditionsView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                // make.bottom.equalToSuperview()
                make.top.equalToSuperview().inset(40)
                make.width.equalToSuperview().dividedBy(1.1)
                make.height.equalTo(50)
            }

            self.documentationButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.bottom.equalToSuperview().inset(5)
            }

            self.termsAndConditionsButton.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(5)
                make.top.equalToSuperview().inset(5)
            }
        }

        let number = self.array[indexPath.row]
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            cell.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.darkGray
        }

        cell.textLabel?.text = number
        cell.textLabel?.textColor = ThemesColors.mainColor
        cell.textLabel?.font = UIFont(name: "Helvetica", size: 12)
        cell.imageView?.image = UIImage(named: number)
        return cell
    }
}
