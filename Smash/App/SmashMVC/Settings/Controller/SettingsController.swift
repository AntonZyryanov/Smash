import ApphudSDK
import SafariServices
import SnapKit
import StoreKit
import UIKit

class SettingsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static var isSubscribeButtonEnabled = false
    let headerView = UIView()
    let backButton = UIButton()
    var myTableView = UITableView()
    let indentifire = "MyCell"
    var array = [
        NSLocalizedString("SUBSCRIPTION", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
        NSLocalizedString("FEEDBACK", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
        "APPSTORE",
        NSLocalizedString("RATE", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
        NSLocalizedString("LEGAL", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
    ]
    var image = UIImageView()
    var settingLabel = UILabel()
    var susbcriptionView = UIView()
    var smashPremium = UILabel()
    var unlockEffects = UILabel()
    var subscribeButton = UIButton()
    var feedbackButton = UIButton()
    var appStoreButton = UIButton()
    var documentationButton = UIButton()
    var rateButton = UIButton()
    // var themeView = UIView()
    var settingsLabel = UILabel()
    // var blackTheme = UILabel()
    var feedbackView = UIView()
    var appstoreView = UIView()
    var rateView = UIView()
    var documentsView = UIView()
    var termsAndConditionsView = UIView()
    var termsAndConditionsButton = UIButton()
    // let switchView = UISwitch()
    static var isThemeIsBlack = false

    var applyMainControllerSnapshotClosure: (([Style]) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.controllerInit()
        // Do any additional setup after loading the view.
    }
}
