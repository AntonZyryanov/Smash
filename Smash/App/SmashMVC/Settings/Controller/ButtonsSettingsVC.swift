import ApphudSDK
import SafariServices
import SnapKit
import StoreKit
import UIKit

extension SettingsController {
    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func subscription() {
        if SettingsController.isSubscribeButtonEnabled == true {
            if Apphud.hasActiveSubscription() == false {
                OnboardingPaywall.isCalledFromAudioVC = true
                let paywallController = OnboardingPaywall()
                self.navigationController?.pushViewController(paywallController, animated: true)
            } else {
                let alert = UIAlertController(
                    title: "",
                    message: NSLocalizedString("You have active subscription", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
                    preferredStyle: .alert
                )
                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(action)
                present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(
                title: NSLocalizedString("Cant check your subscription status", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
                message: NSLocalizedString(
                    "Please check your internet connection and reload app",
                    tableName: "SettingsControllerLocalization",
                    bundle: .main,
                    value: "",
                    comment: ""
                ),
                preferredStyle: .alert
            )
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }

    @objc func rate() {
        guard let scene = self.view.window?.windowScene else {
            return
        }
        if #available(iOS 14.0, *) {
            SKStoreReviewController.requestReview(in: scene)
        } else {
            let alert = UIAlertController(title: NSLocalizedString(
                "Cant rate App",
                tableName: "SettingsControllerLocalization",
                bundle: .main,
                value: "",
                comment: ""
            ), message: NSLocalizedString(
                "Please check your internet connection and reload app",
                tableName: "SettingsControllerLocalization",
                bundle: .main,
                value: "",
                comment: ""
            ), preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }

    @objc func documentation() {
        if let url = URL(string: "https://docs.google.com/document/d/1uLMZXR0j8y9AdXEkvOBV9akIrbvSU4ep/edit?usp=sharing&ouid=109562783417657277282&rtpof=true&sd=true") {
            // UIApplication.shared.open(url)
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true, completion: nil)
        }
    }

    @objc func termsAndConditions() {
        if let url = URL(string: "https://docs.google.com/document/d/1elHuPFOtcEpS12rHNJq7gkMePCiVbhqU/edit?usp=sharing&ouid=109562783417657277282&rtpof=true&sd=true") {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true, completion: nil)
        }
    }

    @objc func appStore() {
        let pasteboard = UIPasteboard.general
        pasteboard.string = UserDefaults.standard.string(forKey: "updateURL")
        print("AppStore link pasted")
        // create the alert
        let alert = UIAlertController(
            title: NSLocalizedString(
                NSLocalizedString("Done", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
                tableName: "SettingsControllerLocalization",
                bundle: .main,
                value: "",
                comment: ""
            ),
            message: NSLocalizedString("AppStore Link copied to clipboard.", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
            preferredStyle: UIAlertController.Style.alert
        )

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @objc func feedback() {
        let pasteboard = UIPasteboard.general
        pasteboard.string = "smasheditorfeedback@gmail.com"
        print("feedback link pasted")
        // create the alert
        let alert = UIAlertController(
            title: NSLocalizedString("Done", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
            message: NSLocalizedString("Feedback e-mail  copied to clipboard.", tableName: "SettingsControllerLocalization", bundle: .main, value: "", comment: ""),
            preferredStyle: UIAlertController.Style.alert
        )

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}
