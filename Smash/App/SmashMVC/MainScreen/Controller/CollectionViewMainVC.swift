import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

// COLLECTION VIEW EXTENSIONS

// MARK: - Collection View Delegate

extension MainController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StyleCell", for: indexPath) as! StyleCell

        // To set the selected cell background color here

        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            cell.backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
        } else {
            cell.backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.addButtonPressed()
        }
        if indexPath.row == 1 {
            self.chosenEffectsButtonPressed()
        }

        guard let style = dataSource.itemIdentifier(for: indexPath) else { return }
        print(style)

        // Main controller effects interaction

        if indexPath.row != 0, indexPath.row != 1 {
            let alert = UIAlertController(
                title: "",
                message: NSLocalizedString("alert_message", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
                preferredStyle: .alert
            )
            let action1 = UIAlertAction(title: NSLocalizedString("alert_yes", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""), style: .cancel) { _ in
                Style.styles[Style.chosenStyles[indexPath.row].id].isSelected = false
                Style.styles[Style.chosenStyles[indexPath.row].id].isEnabled = false

                Style.chosenStyles.remove(at: indexPath.row)
                var array = UserDefaults.standard.stringArray(forKey: "chosenEffectsArray")!
                array.remove(at: indexPath.row - 2) // - 1 because extra cell "Effect Picker" was added to chosenStyle but not to "chosenEffectsArray", so index changed
                UserDefaults.standard.setValue(
                    array,
                    forKey: "chosenEffectsArray"
                )
                self.applySnapshot(styles: Style.chosenStyles)
            }
            let action2 = UIAlertAction(
                title: NSLocalizedString("alert_no", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""),
                style: .default,
                handler: nil
            )
            alert.addAction(action1)
            alert.addAction(action2)
            present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - Collection View Setup

extension MainController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0) // here your custom value for spacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 3 - lay.minimumInteritemSpacing
        //  let heightPerItem = collectionView.frame.height / 3 - lay.minimumInteritemSpacing
        return CGSize(width: widthPerItem, height: widthPerItem)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 1.0
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,

        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 1.0
    }
}

extension MainController {
    enum Section {
        case main
    }

    func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        let groupSize = NSCollectionLayoutSize(widthDimension: .absolute(100), heightDimension: .absolute(100))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.scrollDirection = .vertical // .horizontal
        let layout = UICollectionViewCompositionalLayout(section: section, configuration: config)
        return layout
    }

    func configureCollectionViewLayout() {
        // TODO: collectionView
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.createLayout())
        self.collectionView.delegate = self
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.collectionView.backgroundColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1.0)
        } else {
            self.collectionView.backgroundColor = .darkGray
        }
        // self.collectionView.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        self.collectionView.register(StyleCell.self, forCellWithReuseIdentifier: StyleCell.reuseIdentifier)
    }

    func configureCollectionViewDataSource() {
        // TODO: dataSource
        print("mainController configureCollectionViewDataSource...")
        self.dataSource = DataSource(collectionView: self.collectionView, cellProvider: { (collectionView, indexPath, style) -> StyleCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StyleCell.reuseIdentifier, for: indexPath) as! StyleCell
            cell.configure(with: style)
            cell.setupView()
            //  cell.backgroundColor = ThemesColors.mainColor
            //  cell.backgroundColor = ThemesColors.mainColor
            if indexPath.row < 6 {
                cell.styleImageView.image = self.soundEffects[style.id].image
            } else {
                if Apphud.hasActiveSubscription() {
                    cell.styleImageView.image = self.soundEffects[style.id].image
                } else {
                    // TODO: back
                    cell.styleImageView.image = self.soundEffects[style.id].image
                }
            }
            cell.isService = style.isService
            cell.isLocked = style.isLocked
            cell.isCellMarginIsBlack = UserDefaults.standard.bool(forKey: "isBlackThemeChoosed")
            // cell.highlightCell()
            cell.styleImageView.backgroundColor = ThemesColors.mainColor
            cell.chooseBackgroundColor()
            cell.lockIfNeeded()
            return cell
        })
    }

    func createDummyData(forcePro: Bool = false) {
        var dummyStyles: [Style] = []
        print("Luke Skywalker")
        print(Apphud.hasActiveSubscription())
        for i in 0 ..< 40 {
            dummyStyles.append(Style(
                id: i,
                styleName: self.soundEffects[i].name // "Style \(i)"
            ))
        }

        Style.styles = dummyStyles

        if Apphud.hasActiveSubscription() == false, !forcePro {
            for i in 15 ..< 40 {
                Style.styles[i].isLocked = true
            }
        } else {
            for i in 15 ..< 40 {
                Style.styles[i].isLocked = false
            }
        }
        // add Effect picker cell
        Style.chosenStyles.removeAll()
        Style.chosenStyles.append(Style(id: 41, styleName: "openEditor", isSelected: false, isEnabled: false, isService: true))
        Style.chosenStyles.append(Style(id: 40, styleName: "plus", isSelected: false, isEnabled: false, isService: true))

        // saving chosen effects throughout sessions
        for index in UserDefaults.standard.stringArray(forKey: "chosenEffectsArray")! {
            Style.styles[Int(index)!].isEnabled = true
            Style.chosenStyles.append(Style.styles[Int(index)!])
        }

        //  Style.chosenStyles = dummyStyles
        self.applySnapshot(styles: Style.chosenStyles)
    }

    func fetchItems() {
        guard let url = URL(string: apiUrl) else { return }

        URLSession.shared.dataTask(with: url) { data, response, err in
            if let err = err {
                print(err.localizedDescription)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let fetchedItems = try JSONDecoder().decode([Style].self, from: data)
                        self.applySnapshot(styles: fetchedItems)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            } else {
                print("HTTPURLResponse code: \(response.statusCode)")
            }
        }.resume()
    }

    func applySnapshot(styles: [Style]) { // was private
        // TODO: snapshot
        self.snapshot = DataSourceSnapshot()
        self.snapshot.appendSections([Section.main])
        self.snapshot.appendItems(styles)
        self.dataSource.apply(self.snapshot, animatingDifferences: false)
    }
}
