import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    @objc func showDeniedAccessUI(toWhat: String) {
        var alert = UIAlertController()
        if toWhat == "gallery" {
            alert = UIAlertController(
                title: NSLocalizedString("Allow access to your photos", tableName: "alerts", bundle: .main, value: "", comment: ""),
                message: NSLocalizedString(
                    "Please, let Smash to get access to all your photos to be able to proccess video with amazing audioeffects. Go to your settings and tap \"Photos\".",
                    tableName: "alerts",
                    bundle: .main,
                    value: "",
                    comment: ""
                ),
                preferredStyle: .alert
            )
        }
        if toWhat == "camera" {
            alert = UIAlertController(
                title: NSLocalizedString("Allow access to your camera", tableName: "alerts", bundle: .main, value: "", comment: ""),
                message: NSLocalizedString(
                    "Please, let Smash to get access to your camera to be able to shoot video. Go to your settings and tap \"Camera\".",
                    tableName: "alerts",
                    bundle: .main,
                    value: "",
                    comment: ""
                ),
                preferredStyle: .alert
            )
        }

        if toWhat == "microphone" {
            alert = UIAlertController(
                title: NSLocalizedString("Allow access to your microphone", tableName: "alerts", bundle: .main, value: "", comment: ""),
                message: NSLocalizedString(
                    "Please, let Smash to get access to your microphone to be able to shoot video. Go to your settings and tap \"Microphone\".",
                    tableName: "alerts",
                    bundle: .main,
                    value: "",
                    comment: ""
                ),
                preferredStyle: .alert
            )
        }

        let notNowAction = UIAlertAction(
            title: NSLocalizedString("Not Now", tableName: "alerts", bundle: .main, value: "", comment: ""),
            style: .cancel,
            handler: nil
        )
        alert.addAction(notNowAction)

        let openSettingsAction = UIAlertAction(
            title: NSLocalizedString("Open Settings", tableName: "alerts", bundle: .main, value: "", comment: ""),
            style: .default
        ) { [unowned self] _ in
            // Open app privacy settings
            gotoAppPrivacySettings()
        }
        alert.addAction(openSettingsAction)

        present(alert, animated: true, completion: nil)
    }

    func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
              UIApplication.shared.canOpenURL(url)
        else {
            assertionFailure("Not able to open App privacy settings")
            return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
