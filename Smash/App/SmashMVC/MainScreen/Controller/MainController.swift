import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

class MainController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    // UI
    let addButton = UIButton()
    let settingsButton = UIButton()
    let chosenEffectsButton = UIButton()
    let photoshootButton = UIButton()
    let advertisementButton = UIButton()
    let lowPanel = UIView()
    let mainLogo = UIImageView()
    var galleryButtonLogo = UIImageView()
    var photoshootButtonLogo = UIImageView()
    var settingsButtonLogo = UIImageView()
    // OPTIONAL UI
    var tutorialContinueButton = UIButton()
    var tutorialFirstArrow = UIImageView()
    var tutorialSecondArrow = UIImageView()
    var tutorialTitle = UILabel()
    var tutorialBackground = UIView()
    // VIDEO PICKING
    let imagePickerController = UIImagePickerController()
    var videoURL: NSURL?
    // COLLECTION VIEW
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Style>
    typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, Style>
    var collectionView: UICollectionView!
    let apiUrl = "https://jsonplaceholder.typicode.com/users"
    var dataSource: DataSource!
    var snapshot = DataSourceSnapshot()
    // SOUND EFFECTS
    let soundEffects = SoundEffect.loadSoundEffects()
    // LOGICAL VARIABLES
    var isEffectsPanelShown: Bool = true
    var isUpdateAlertPassed: Bool = false
    // GOOGLE ADS
    let banner: GADBannerView = {
        let banner = GADBannerView(adSize: GADAdSizeFromCGSize(CGSize(width: 320, height: 50)))

        return banner
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.userBoughtSubscription), name: Notification.Name("userBoughtSubscription"), object: nil)
        configureCollectionViewLayout()
        self.controllerInit()

        configureCollectionViewDataSource()
        createDummyData()

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical // .horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.collectionView.setCollectionViewLayout(layout, animated: true)
        if UserDefaults.standard.bool(forKey: "isTutorialPassed") == false {
            self.tutorial()
        }

        if UserDefaults.standard.bool(forKey: "isThereIsUpdate") == true, self.isUpdateAlertPassed == false {
            let updateAlert = UIAlertController(title: "There was an update", message: "Please download it", preferredStyle: .alert)
            let updateAlertAction = UIAlertAction(title: "Go to AppStore", style: .cancel) { _ in
                print("sheesh")
                if let url = URL(string: UserDefaults.standard.string(forKey: "updateURL")!) {
                    UIApplication.shared.open(url)
                }
            }
            updateAlert.addAction(updateAlertAction)
            self.present(updateAlert, animated: true) {
                self.isUpdateAlertPassed = true
            }
        }

        self.banner.rootViewController = self
        self.banner.adUnitID = "ca-app-pub-1329450099126170/8233561396"

        self.banner.load(GADRequest())
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    func loadChosenEffects() {
        UserDefaults.standard.stringArray(forKey: "chosenEffectsArray")
    }

    @objc func userBoughtSubscription() {
        self.controllerInit()
        createDummyData(forcePro: true)
    }

    @objc func themeChanged() {
        self.controllerInit()
    }
}
