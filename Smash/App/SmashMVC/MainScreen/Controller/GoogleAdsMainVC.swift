import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController: GADBannerViewDelegate {
    func bannerViewDidRecordClick(_ bannerView: GADBannerView) {
        print("User tapped on advisory banner")
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.userTappedOnBanner.rawValue)
    }
}
