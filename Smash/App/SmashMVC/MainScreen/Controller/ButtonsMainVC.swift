import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    @objc func settingsButtonPressed() {
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.mainVCSettingsButtonPressed.rawValue)
        let settingsController = SettingsController()
        settingsController.applyMainControllerSnapshotClosure = applySnapshot(styles:)

        self.navigationController?.pushViewController(settingsController, animated: true)
    }

    @objc func addButtonPressed() {
        if #available(iOS 14, *) {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { [unowned self] status in
                DispatchQueue.main.async { [unowned self] in
                    switch status {
                    case .authorized:
                        self.presentImagePicker()
                        print("1")

                    case .limited:
                        //       showLimittedAccessUI()
                        showDeniedAccessUI(toWhat: "gallery")
                        print("2")
                    case .restricted:
                        //  showRestrictedAccessUI()
                        showDeniedAccessUI(toWhat: "gallery")
                        print("2")
                    case .denied:
                        //         showAccessDeniedUI()
                        showDeniedAccessUI(toWhat: "gallery")
                    case .notDetermined:
                        break

                    @unknown default:
                        break
                    }
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization { status in
                DispatchQueue.main.async {
                    if status == .authorized {
                        self.presentImagePicker()
                    } else {
                        self.showDeniedAccessUI(toWhat: "gallery")
                    }
                }
            }
        }
    }

    @objc func photoshootButtonPressed() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in

            DispatchQueue.main.async {
                if response {
                    // Request permission to record.
                    AVAudioSession.sharedInstance().requestRecordPermission { granted in
                        DispatchQueue.main.async {
                            if granted {
                                self.shootVideo()
                            } else {
                                self.showDeniedAccessUI(toWhat: "microphone")
                            }
                        }
                    }
                } else {
                    self.showDeniedAccessUI(toWhat: "camera")
                }
            }
        }
    }

    @objc func chosenEffectsButtonPressed() {
        let effectsPickerController = EffectsPickerController()
        effectsPickerController.applyMainControllerSnapshotClosure = applySnapshot(styles:)
        effectsPickerController.modalPresentationStyle = .pageSheet
        effectsPickerController.modalTransitionStyle = .crossDissolve
        effectsPickerController.isCalledFromMainController = true
        present(effectsPickerController, animated: true)
    }

    func shootVideo() {
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.mainVCPhotoshootButtonPressed.rawValue)
        self.imagePickerController.sourceType = .camera
        self.imagePickerController.allowsEditing = true
        self.imagePickerController.delegate = self
        self.imagePickerController.mediaTypes = ["public.movie"]

        present(self.imagePickerController, animated: true, completion: nil)
    }
}
