import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    @objc func tutorialContinueButtonPressed() {
        self.addButton.isEnabled = true
        self.photoshootButton.isEnabled = true
        self.tutorialFirstArrow.removeFromSuperview()
        self.tutorialSecondArrow.removeFromSuperview()
        self.tutorialBackground.removeFromSuperview()
        self.tutorialContinueButton.removeFromSuperview()
        self.tutorialTitle.removeFromSuperview()
        UserDefaults.standard.setValue(true, forKey: "isTutorialPassed")
    }
}
