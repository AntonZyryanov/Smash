import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    func presentImagePicker() {
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.mainVCGalleryButtonPressed.rawValue)
        self.imagePickerController.sourceType = .photoLibrary
        self.imagePickerController.delegate = self
        self.imagePickerController.mediaTypes = ["public.movie"]
        present(self.imagePickerController, animated: true, completion: {
            self.imagePickerController.navigationBar.tintColor = ThemesColors.mainColor
        })
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        // videoURL = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerReferenceURL")]  as? NSURL
        // print(videoURL!)

        self.imagePickerController.dismiss(animated: true, completion: nil)

        if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            // Do something with the URL
            print(url)
            let videoData = try? Data(contentsOf: url)
            if videoData == nil {
                print("nil")
            }

            var name = url.lastPathComponent // extra
            print(name)

            let tmpDirectory = URL(fileURLWithPath: NSTemporaryDirectory()) // extra

            let targetvideoURLString = tmpDirectory.absoluteString + name // ex

            try? videoData?.write(to: URL(string: targetvideoURLString)!)

            // video to audio processing
            name.removeLast(4)
            let audioName: String = name + ".WAV"
            let videoName: String = name + ".MOV"
            print(audioName)

            let asset = AVURLAsset(url: url)
            let targetImgeURLString = tmpDirectory.absoluteString + audioName // ex

            let videoURLString = tmpDirectory.absoluteString + videoName // ex

            if let targetImgeURL = URL(string: targetImgeURLString) {
                try? MainController.writeAudioTrackToUrl(asset: asset, targetImgeURL)
            }

            // AudioEditorController launch

            let audioEditorController = AudioEditorController()

            //  self.present(audioEditorController, animated: true, completion: nil)
            audioEditorController.recordedAudioURL = URL(string: targetImgeURLString)
            audioEditorController.recordedVideoURL = URL(string: videoURLString)
            audioEditorController.applyMainControllerSnapshotClosure = applySnapshot(styles:)
            audioEditorController.applyBackToMainControllerSnapshotClosure = createDummyData(forcePro:)

            self.navigationController?.pushViewController(audioEditorController, animated: true)
        }
    }
}
