import AVFoundation
import AVKit
import Foundation

extension MainController {
    static func writeAudioTrackToUrl(asset: AVAsset, _ url: URL) throws {
        // initialize asset reader, writer
        let assetReader = try AVAssetReader(asset: asset)
        let assetWriter = try AVAssetWriter(outputURL: url, fileType: .wav)

        // get audio track
        let audioTrack = asset.tracks(withMediaType: AVMediaType.audio).first!

        // configure output audio settings
        let audioSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatLinearPCM,
            AVSampleRateKey: 22050.0,
            AVNumberOfChannelsKey: 1,
            AVLinearPCMBitDepthKey: 16,
            AVLinearPCMIsFloatKey: false,
            AVLinearPCMIsBigEndianKey: false,
            AVLinearPCMIsNonInterleaved: false,
        ]
        let assetReaderAudioOutput = AVAssetReaderTrackOutput(track: audioTrack, outputSettings: audioSettings)

        if assetReader.canAdd(assetReaderAudioOutput) {
            assetReader.add(assetReaderAudioOutput)
        } else {
            fatalError("could not add audio output reader")
        }

        let _: [String: Any] = [AVFormatIDKey: kAudioFormatLinearPCM]
        let audioInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioSettings)

        let audioInputQueue = DispatchQueue(label: "audioQueue")
        assetWriter.add(audioInput)

        assetWriter.startWriting()
        assetReader.startReading()
        assetWriter.startSession(atSourceTime: CMTime.zero)

        audioInput.requestMediaDataWhenReady(on: audioInputQueue) {
            while audioInput.isReadyForMoreMediaData {
                let sample = assetReaderAudioOutput.copyNextSampleBuffer()
                if sample != nil {
                    audioInput.append(sample!)
                } else {
                    audioInput.markAsFinished()
                    DispatchQueue.main.async {
                        assetWriter.finishWriting {
                            assetReader.cancelReading()
                        }
                    }
                    break
                }
            }
        }
    }
}
