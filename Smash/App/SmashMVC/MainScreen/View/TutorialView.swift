import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    func tutorial() {
        self.addButton.isEnabled = false
        self.photoshootButton.isEnabled = false
        _ = UIView()
        self.tutorialFirstArrow.image = UIImage(named: "arrow")
        self.tutorialSecondArrow.image = UIImage(named: "arrow")
        self.tutorialBackground.backgroundColor = UIColor(red: 256 / 256, green: 256 / 256, blue: 256 / 256, alpha: 0.6)
        self.tutorialContinueButton.setTitle(NSLocalizedString("Got it", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        self.tutorialContinueButton.backgroundColor = UIColor.black
        self.tutorialContinueButton.layer.masksToBounds = true
        self.tutorialContinueButton.layer.cornerRadius = 8.0
        self.tutorialContinueButton.addTarget(self, action: #selector(self.tutorialContinueButtonPressed), for: .touchUpInside)
        view.addSubview(self.tutorialBackground)
        view.addSubview(self.addButton)
        view.addSubview(self.photoshootButton)
        view.addSubview(self.tutorialFirstArrow)
        view.addSubview(self.tutorialSecondArrow)
        view.addSubview(self.tutorialContinueButton)
        view.addSubview(self.tutorialTitle)
        self.tutorialTitle.numberOfLines = 0
        self.tutorialTitle.textColor = UIColor.black
        self.tutorialTitle.sizeToFit()
        self.tutorialTitle.font = UIFont.systemFont(ofSize: 22.0, weight: .bold)
        self.tutorialTitle.textAlignment = .center
        self.tutorialTitle.text = NSLocalizedString("prompt", tableName: "OnBoardingsLocalization", bundle: .main, value: "", comment: "")
        self.tutorialTitle.numberOfLines = 0
        self.tutorialBackground.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        }
        self.tutorialFirstArrow.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(addButton.snp_topMargin)
            make.height.equalTo(100)
            make.width.equalTo(40)
        }
        self.tutorialSecondArrow.snp.makeConstraints { make in
            make.centerX.equalTo(photoshootButton.snp_centerXWithinMargins)
            make.bottom.equalTo(photoshootButton.snp_topMargin)
            make.height.equalTo(100)
            make.width.equalTo(40)
        }
        self.tutorialContinueButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(200)
        }

        self.tutorialTitle.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.tutorialContinueButton.snp_topMargin).inset(-50)
            make.height.equalTo(200)
            make.width.equalTo(300)
        }
    }
}
