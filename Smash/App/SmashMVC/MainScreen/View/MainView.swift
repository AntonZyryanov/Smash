import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import UIKit

extension MainController {
    func controllerInit(forcePro: Bool = false) {
        self.navigationController?.navigationBar.isHidden = true
        self.title = "VIDEO LIBRARY"
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            view.backgroundColor = UIColor.white
        } else {
            view.backgroundColor = UIColor.gray
        }
        view.addSubview(self.lowPanel)
        view.addSubview(self.addButton)
        view.addSubview(self.settingsButton)
        view.addSubview(self.photoshootButton)
        view.addSubview(self.collectionView)

        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.lowPanel.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            self.lowPanel.backgroundColor = UIColor.darkGray
        }

        self.lowPanel.layer.cornerRadius = 20
        self.lowPanel.clipsToBounds = true
        self.lowPanel.contentMode = .scaleAspectFill
        self.addButton.addTarget(self, action: #selector(self.addButtonPressed), for: .touchUpInside)
        self.addButton.layer.masksToBounds = true
        self.addButton.layer.cornerRadius = 40
        self.addButton.clipsToBounds = true
        self.addButton.tintColor = UIColor.white
        self.addButton.backgroundColor = ThemesColors.mainColor
        self.addButton.contentMode = .scaleAspectFit
        self.addButton.addSubview(self.galleryButtonLogo)
        self.galleryButtonLogo.image = UIImage(named: "galleryButton")
        self.photoshootButton.addTarget(self, action: #selector(self.photoshootButtonPressed), for: .touchUpInside)

        self.photoshootButton.tintColor = UIColor.white
        self.photoshootButton.backgroundColor = ThemesColors.mainColor
        self.photoshootButton.contentMode = .scaleAspectFit
        self.photoshootButton.addSubview(self.photoshootButtonLogo)
        self.photoshootButtonLogo.image = UIImage(named: "cameraButton")

        self.settingsButton.tintColor = UIColor.white
        self.settingsButton.addTarget(self, action: #selector(self.settingsButtonPressed), for: .touchUpInside)
        self.settingsButton.contentMode = .scaleAspectFit
        self.settingsButton.backgroundColor = ThemesColors.mainColor
        self.settingsButton.addSubview(self.settingsButtonLogo)
        self.settingsButtonLogo.image = UIImage(named: "settingsButton")

        self.chosenEffectsButton.setImage(UIImage(named: "settingsButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.chosenEffectsButton.tintColor = UIColor.white
        self.chosenEffectsButton.addTarget(self, action: #selector(self.chosenEffectsButtonPressed), for: .touchUpInside)
        self.chosenEffectsButton.contentMode = .scaleAspectFit
        self.chosenEffectsButton.backgroundColor = ThemesColors.mainColor

        self.advertisementButton.setTitle("Advertisment Advertisment Advertisment", for: .normal)

        self.advertisementButton.backgroundColor = .systemRed

        self.mainLogo.backgroundColor = ThemesColors.mainColor
        self.mainLogo.layer.cornerRadius = 150
        self.mainLogo.layer.masksToBounds = true

        self.lowPanel.snp.makeConstraints { make in

            make.bottom.equalToSuperview().inset(-20)
            make.width.equalToSuperview()
            make.height.equalTo(190)
        }

        self.settingsButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(40)
            make.bottom.equalToSuperview().inset(45)
            make.height.width.equalTo(80.0)
        }

        self.settingsButtonLogo.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40.0)
        }

        self.addButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(45)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(80.0)
        }

        self.galleryButtonLogo.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.width.equalTo(36.0)
        }

        self.photoshootButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(40)
            make.bottom.equalToSuperview().inset(45)
            make.height.width.equalTo(80.0)
        }

        self.photoshootButtonLogo.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40.0)
        }

        self.collectionView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(170)
            make.top.equalToSuperview()
        }

        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.mainLogo.image = UIImage(named: "mainLogo")
        } else {
            self.mainLogo.image = UIImage(named: "mainLogoBlack")
        }

        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.collectionView.backgroundColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1.0)
        } else {
            self.collectionView
                .backgroundColor = UIColor(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0)
        }

        self.settingsButton.layer.cornerRadius = 40
        self.settingsButton.clipsToBounds = true

        self.chosenEffectsButton.layer.cornerRadius = 40
        self.chosenEffectsButton.clipsToBounds = true

        self.photoshootButton.layer.cornerRadius = 40
        self.photoshootButton.clipsToBounds = true

        // banner
        self.banner.rootViewController = self
        self.banner.backgroundColor = UIColor(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 0)
        if Apphud.hasActiveSubscription() == false {
            view.addSubview(self.banner)

            self.banner.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.width.equalTo(320)
                make.height.equalTo(44)
                make.centerX.equalToSuperview()
            }
        }

        self.banner.delegate = self
    }
}
