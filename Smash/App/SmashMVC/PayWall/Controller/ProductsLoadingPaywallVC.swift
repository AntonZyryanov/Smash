import ApphudSDK
import SafariServices
import SnapKit
import UIKit

extension OnboardingPaywall {
    @objc func productsLoaded() {
        self.yearSubDiscount
            .text = "-" +
            String(
                100 - Int(100 * (PurchasesProcessings.products?[0].skProduct?.price.doubleValue)! / (((PurchasesProcessings.products?[1].skProduct?.price.doubleValue)!) * 12))
            ) +
            " %"

        if let price1 = PurchasesProcessings.products?[1].skProduct?.priceLocale {
            self.yearSubMonthPrice.text = String(((PurchasesProcessings.products?[0].skProduct?.price.doubleValue)! / 12).rounded(toPlaces: 2)) + " " + price1.currencySymbol!
        }

        if let price2 = PurchasesProcessings.products?[1].skProduct?.price, let price2Locale = PurchasesProcessings.products?[1].skProduct?.priceLocale {
            self.monthSubConditions.text = price2.stringValue + " " + price2Locale
                .currencySymbol! + NSLocalizedString(" / month", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        }

        if let price3 = PurchasesProcessings.products?[0].skProduct?.price, let price3Locale = PurchasesProcessings.products?[0].skProduct?.priceLocale {
            self.yearSubConditions.text = price3.stringValue + " " + price3Locale.currencySymbol! + NSLocalizedString(
                " / year",
                tableName: "PayWallLocalization",
                bundle: .main,
                value: "",
                comment: ""
            )
        }

        if let price4Locale = PurchasesProcessings.products?[1].skProduct?.priceLocale {
            self.yearSubMonthPrice.text = String(((PurchasesProcessings.products?[0].skProduct?.price.doubleValue)! / 12).rounded(toPlaces: 2)) + " " + price4Locale.currencySymbol!
        }

        self.actionButton.isEnabled = true

        self.activityMonitor.stopAnimating()
    }
}
