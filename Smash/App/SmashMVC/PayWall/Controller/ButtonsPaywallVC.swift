import ApphudSDK
import SafariServices
import SnapKit
import UIKit

extension OnboardingPaywall {
    @objc func chooseMonthSub() {
        self.isYearSubChosen = false
        self.createElements()
    }

    @objc func chooseYearSub() {
        self.isYearSubChosen = true
        self.createElements()
    }

    @objc func closeSubscriptionScreen() {
        if OnboardingPaywall.isCalledFromAudioVC == false {
            AppDelegate.shared.router.showMainFlow()
        } else {
            //  self.applyAudioEditorControllerSnapshotClosure!(Style.chosenStyles)
            // self.applyMainControllerSnapshotClosure!(Style.chosenStyles)
            self.navigationController?.popViewController(animated: false)
        }
    }

    @objc func restoreSubscription() {
        Apphud.restorePurchases { _, _, _ in
            if Apphud.hasActiveSubscription() {
                // has active subscription
            } else {
                // no active subscription found, check non-renewing purchases or error
            }
        }
    }

    @objc func termsButtonClicked() {
        if let url = URL(string: "https://docs.google.com/document/d/1uLMZXR0j8y9AdXEkvOBV9akIrbvSU4ep/edit?usp=sharing&ouid=109562783417657277282&rtpof=true&sd=true") {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true, completion: nil)
        }
    }

    @objc func conditionsButtonClicked() {
        if let url = URL(string: "https://docs.google.com/document/d/1elHuPFOtcEpS12rHNJq7gkMePCiVbhqU/edit?usp=sharing&ouid=109562783417657277282&rtpof=true&sd=true") {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true, completion: nil)
        }
    }

    @objc func subscribe() {
        self.activityMonitor.startAnimating()

        if self.isYearSubChosen == true {
            Apphud.purchase(PurchasesProcessings.products![0]) { result in
                if let subscription = result.subscription, subscription.isActive() {
                    // has active subscription
                    SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.yearSubscription.rawValue)
                    print("Chupa chups")
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("userBoughtSubscription"), object: nil)
                } else if let purchase = result.nonRenewingPurchase, purchase.isActive() {
                    // has active non-renewing purchase
                } else {
                    // handle error or check transaction status.
                }
                self.activityMonitor.stopAnimating()
            }
        } else {
            Apphud.purchase(PurchasesProcessings.products![1]) { result in
                if let subscription = result.subscription, subscription.isActive() {
                    // has active subscription
                    SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.monthSubscription.rawValue)
                    print("Chupa chups")
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("userBoughtSubscription"), object: nil)
                } else if let purchase = result.nonRenewingPurchase, purchase.isActive() {
                    // has active non-renewing purchase
                } else {
                    // handle error or check transaction status.
                }
                self.activityMonitor.stopAnimating()
            }
        }
    }
}
