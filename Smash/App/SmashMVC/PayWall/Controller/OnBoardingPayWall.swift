import ApphudSDK
import SafariServices
import SnapKit
import UIKit

class OnboardingPaywall: UIViewController {
    static var isCalledFromAudioVC = false

    var applyMainControllerSnapshotClosure: (([Style]) -> Void)?

    var applyAudioEditorControllerSnapshotClosure: (([Style]) -> Void)?

    var isYearSubChosen = true

    let backgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = Bundle.main.loadImage(withName: "paywall.jpg")
        return imageView
    }()

    let upperView: UIView = {
        let upperView = UIView()
        upperView.backgroundColor = .clear
        return upperView
    }()

    let barHeight: CGFloat = 80.0
    let conditionBarHeight: CGFloat = 76.0
    let actionButtonHeight: CGFloat = 56.0
    let inset: CGFloat = 15.0
    let subscriptionContainerHeight: CGFloat = 100.0

    var activityMonitor = UIActivityIndicatorView(style: .large)

    let closeButton: UIButton = {
        let closeButton = UIButton()
        closeButton.addTarget(self, action: #selector(closeSubscriptionScreen), for: .touchDown)
        closeButton.setTitle("X", for: .normal)
        closeButton.isUserInteractionEnabled = true
        closeButton.setTitleColor(ThemesColors.whiteColor, for: .normal)
        return closeButton
    }()

    let restoreButton: UIButton = {
        let restoreButton = UIButton()
        restoreButton.addTarget(self, action: #selector(restoreSubscription), for: .touchDown)
        restoreButton.setTitle(NSLocalizedString("Restore Purchases", tableName: "PayWallLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        restoreButton.isUserInteractionEnabled = true
        restoreButton.setTitleColor(ThemesColors.whiteColor, for: .normal)
        restoreButton.contentHorizontalAlignment = .right
        return restoreButton
    }()

    let containerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .clear
        return containerView
    }()

    let premiumTitleLabel: UILabel = {
        let premiumTitle = UILabel()
        premiumTitle.text = NSLocalizedString("premium title", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        premiumTitle.textColor = ThemesColors.whiteColor
        premiumTitle.sizeToFit()
        premiumTitle.font = UIFont.systemFont(ofSize: 34.0, weight: .bold)
        premiumTitle.textAlignment = .center
        premiumTitle.numberOfLines = 2
        return premiumTitle
    }()

    let advantageOneLabel: UILabel = {
        let advantageOneLabel = UILabel()
        advantageOneLabel.text = NSLocalizedString("► Full package of audio effects", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        advantageOneLabel.textColor = ThemesColors.whiteColor
        advantageOneLabel.sizeToFit()
        advantageOneLabel.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
        advantageOneLabel.textAlignment = .left
        return advantageOneLabel
    }()

    let advantageTwoLabel: UILabel = {
        let advantageTwoLabel = UILabel()
        advantageTwoLabel.text = NSLocalizedString("► No advertisement", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        advantageTwoLabel.textColor = ThemesColors.whiteColor
        advantageTwoLabel.sizeToFit()
        advantageTwoLabel.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
        advantageTwoLabel.textAlignment = .left
        return advantageTwoLabel
    }()

    let advantageThreeLabel: UILabel = {
        let advantageThreeLabel = UILabel()
        advantageThreeLabel.text = ""
        advantageThreeLabel.textColor = ThemesColors.whiteColor
        advantageThreeLabel.sizeToFit()
        return advantageThreeLabel
    }()

    let monthSubContainerShadow: UIView = {
        let monthSubContainerShadow = UIView()
        monthSubContainerShadow.backgroundColor = .white
        monthSubContainerShadow.layer.masksToBounds = false
        monthSubContainerShadow.layer.cornerRadius = 25.0
        monthSubContainerShadow.layer.shadowColor = UIColor.black.cgColor
        monthSubContainerShadow.layer.shadowPath = UIBezierPath(roundedRect: monthSubContainerShadow.bounds, cornerRadius: monthSubContainerShadow.layer.cornerRadius).cgPath
        monthSubContainerShadow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        monthSubContainerShadow.layer.shadowOpacity = 0.5
        monthSubContainerShadow.layer.shadowRadius = 1.0
        monthSubContainerShadow.layer.borderColor = UIColor.lightGray.cgColor
        monthSubContainerShadow.isUserInteractionEnabled = true
        //  let gesture = UITapGestureRecognizer(target: self, action: #selector(chooseMonthSub))
        //  monthSubContainerShadow.addGestureRecognizer(gesture)
        return monthSubContainerShadow
    }()

    lazy var monthSubContainer: UIView = {
        let monthSubContainer = UIView()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(chooseMonthSub))
        monthSubContainer.addGestureRecognizer(gesture)
        monthSubContainer.isUserInteractionEnabled = true
        return monthSubContainer
    }()

    let monthSubCheckBorder: UIView = {
        let monthSubCheckBorder = UIView()
        monthSubCheckBorder.layer.borderColor = Colors.mainColor.cgColor
        monthSubCheckBorder.layer.borderWidth = 2.0
        return monthSubCheckBorder
    }()

    let monthSubCheckCenter = UIView()

    let monthSubAction: UILabel = {
        let monthSubAction = UILabel()
        monthSubAction.text = NSLocalizedString("monthly subscription", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        monthSubAction.font = UIFont(name: monthSubAction.font.fontName, size: 14)
        monthSubAction.numberOfLines = 0
        monthSubAction.textAlignment = .left
        monthSubAction.lineBreakMode = .byWordWrapping
        monthSubAction.font = UIFont.systemFont(ofSize: 18.0, weight: .semibold)
        monthSubAction.textColor = Colors.mainColor
        return monthSubAction
    }()

    let monthSubConditions: UILabel = {
        let monthSubConditions = UILabel()
        monthSubConditions.text = "Loading..."

        monthSubConditions.font = UIFont(name: monthSubConditions.font.fontName, size: 15)
        monthSubConditions.numberOfLines = 0
        monthSubConditions.textAlignment = .left
        monthSubConditions.lineBreakMode = .byWordWrapping
        monthSubConditions.textColor = Colors.mainColor
        return monthSubConditions
    }()

    let yearSubContainerShadow: UIView = {
        let yearSubContainerShadow = UIView()
        yearSubContainerShadow.backgroundColor = .white
        yearSubContainerShadow.layer.masksToBounds = false
        yearSubContainerShadow.layer.cornerRadius = 25.0
        yearSubContainerShadow.layer.shadowColor = UIColor.black.cgColor
        yearSubContainerShadow.layer.shadowPath = UIBezierPath(roundedRect: yearSubContainerShadow.bounds, cornerRadius: yearSubContainerShadow.layer.cornerRadius).cgPath
        yearSubContainerShadow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        yearSubContainerShadow.layer.shadowOpacity = 0.5
        yearSubContainerShadow.layer.shadowRadius = 1.0
        yearSubContainerShadow.layer.borderColor = UIColor.lightGray.cgColor
        yearSubContainerShadow.isUserInteractionEnabled = true
        //   yearSubContainerShadow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseYearSub)))
        return yearSubContainerShadow
    }()

    lazy var yearSubContainer: UIView = {
        let yearSubContainer = UIView()

        yearSubContainer.isUserInteractionEnabled = true
        yearSubContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseYearSub)))
        return yearSubContainer
    }()

    let yearSubCheckBorder: UIView = {
        let yearSubCheckBorder = UIView()
        yearSubCheckBorder.layer.borderColor = Colors.mainColor.cgColor
        yearSubCheckBorder.layer.borderWidth = 2.0
        return yearSubCheckBorder
    }()

    let yearSubCheckCenter = UIView()

    let yearSubAction: UILabel = {
        let yearSubAction = UILabel()
        yearSubAction.text = NSLocalizedString("annual subscription", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        yearSubAction.font = UIFont(name: yearSubAction.font.fontName, size: 14)
        yearSubAction.numberOfLines = 0
        yearSubAction.textAlignment = .left
        yearSubAction.lineBreakMode = .byWordWrapping
        yearSubAction.textColor = Colors.mainColor
        yearSubAction.font = UIFont.systemFont(ofSize: 18.0, weight: .semibold)
        return yearSubAction
    }()

    let yearSubConditions: UILabel = {
        let yearSubConditions = UILabel()
        yearSubConditions.text = "Loading..."
        yearSubConditions.font = UIFont(name: yearSubConditions.font.fontName, size: 15)
        yearSubConditions.numberOfLines = 0
        yearSubConditions.textAlignment = .left
        yearSubConditions.lineBreakMode = .byWordWrapping
        yearSubConditions.textColor = Colors.mainColor
        return yearSubConditions
    }()

    let yearSubSeparator = UILabel()
    let yearSubMonthLabel: UILabel = {
        let yearSubMonthLabel = UILabel()
        yearSubMonthLabel.text = NSLocalizedString("per month", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        yearSubMonthLabel.font = UIFont(name: yearSubMonthLabel.font.fontName, size: 10)
        yearSubMonthLabel.numberOfLines = 0
        yearSubMonthLabel.textAlignment = .center
        yearSubMonthLabel.lineBreakMode = .byWordWrapping
        yearSubMonthLabel.textColor = Colors.mainColor
        return yearSubMonthLabel
    }()

    let yearSubMonthPrice: UILabel = {
        let yearSubMonthPrice = UILabel()
        yearSubMonthPrice.text = "Loading..."
        yearSubMonthPrice.font = UIFont(name: yearSubMonthPrice.font.fontName, size: 10)
        yearSubMonthPrice.numberOfLines = 0
        yearSubMonthPrice.textAlignment = .center
        yearSubMonthPrice.lineBreakMode = .byWordWrapping
        yearSubMonthPrice.textColor = Colors.mainColor
        return yearSubMonthPrice
    }()

    let yearSubDiscount: UILabel = {
        let yearSubDiscount = UILabel()
        yearSubDiscount
            .text = "..."
        yearSubDiscount.font = UIFont(name: yearSubDiscount.font.fontName, size: 22)
        yearSubDiscount.numberOfLines = 0
        yearSubDiscount.textAlignment = .center
        yearSubDiscount.lineBreakMode = .byWordWrapping
        yearSubDiscount.textColor = UIColor.red
        return yearSubDiscount
    }()

    let trialLabel: UILabel = {
        let yearSubDiscount = UILabel()
        yearSubDiscount
            .text = NSLocalizedString("trialLabel", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        yearSubDiscount.font = UIFont(name: yearSubDiscount.font.fontName, size: 10)
        yearSubDiscount.numberOfLines = 0
        yearSubDiscount.textAlignment = .center
        yearSubDiscount.lineBreakMode = .byWordWrapping
        yearSubDiscount.textColor = UIColor.red
        return yearSubDiscount
    }()

    let conditionsLabel: UILabel = {
        let conditionsLabel = UILabel()
        conditionsLabel
            .text =
            NSLocalizedString("conditions", tableName: "PayWallLocalization", bundle: .main, value: "", comment: "")
        conditionsLabel.font = UIFont(name: conditionsLabel.font.fontName, size: 9)
        conditionsLabel.numberOfLines = 0
        conditionsLabel.textAlignment = .center
        conditionsLabel.lineBreakMode = .byWordWrapping
        conditionsLabel.textColor = .lightGray
        return conditionsLabel
    }()

    let actionButton: UIButton = {
        let actionButton = UIButton()
        actionButton.backgroundColor = Colors.mainColor
        actionButton.setTitle(NSLocalizedString("sub", tableName: "PayWallLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
        actionButton.layer.borderColor = UIColor.white.cgColor
        actionButton.layer.borderWidth = 1.0
        actionButton.addTarget(self, action: #selector(subscribe), for: .touchDown)
        actionButton.isEnabled = false
        return actionButton
    }()

    let termsLabel: UILabel = {
        let termsLabel = UILabel()
        termsLabel
            .text =
            ""
        termsLabel.font = UIFont(name: termsLabel.font.fontName, size: 10)
        termsLabel.numberOfLines = 0
        termsLabel.textAlignment = .center
        termsLabel.lineBreakMode = .byWordWrapping
        termsLabel.textColor = .lightGray
        return termsLabel
    }()

    let conditionContainer = UIView()

    let termsButton: UIButton = {
        let termsButton = UIButton()
        termsButton.addTarget(self, action: #selector(termsButtonClicked), for: .touchDown)
        termsButton.setTitle(NSLocalizedString("privacy_policy", tableName: "PayWallLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        termsButton.sizeToFit()
        termsButton.isUserInteractionEnabled = true
        termsButton.setTitleColor(ThemesColors.whiteColor, for: .normal)
        termsButton.titleLabel?.numberOfLines = 2
        termsButton.titleLabel?.textAlignment = .center
        return termsButton
    }()

    let conditionsButton: UIButton = {
        let conditionsButton = UIButton()
        conditionsButton.addTarget(self, action: #selector(conditionsButtonClicked), for: .touchDown)
        conditionsButton.setTitle(NSLocalizedString("terms_and_conditions", tableName: "PayWallLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        conditionsButton.sizeToFit()
        conditionsButton.isUserInteractionEnabled = true
        conditionsButton.setTitleColor(ThemesColors.whiteColor, for: .normal)
        conditionsButton.titleLabel?.textAlignment = .center
        conditionsButton.titleLabel?.numberOfLines = 2
        // conditionsButton
        return conditionsButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityMonitor.startAnimating()
        view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true

        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.productsLoaded), name: Notification.Name("productsLoaded"), object: nil)

        self.initElements()
        self.createElements()
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.paywallPassed.rawValue)
        PurchasesProcessings.loadProducts()
    }
}
