import ApphudSDK
import SafariServices
import SnapKit
import UIKit

extension OnboardingPaywall {
    func initElements() {
        view.addSubview(self.backgroundImage)
        view.addSubview(self.upperView)
        self.upperView.addSubview(self.closeButton)
        self.upperView.addSubview(self.restoreButton)
        view.isUserInteractionEnabled = true
        view.addSubview(self.containerView)
        self.containerView.addSubview(self.premiumTitleLabel)
        self.containerView.addSubview(self.advantageOneLabel)
        self.containerView.addSubview(self.advantageTwoLabel)
        self.containerView.addSubview(self.advantageThreeLabel)

        self.containerView.addSubview(self.monthSubContainerShadow)
        self.monthSubContainerShadow.addSubview(self.yearSubContainer)
        self.monthSubContainer.addSubview(self.monthSubAction)
        self.monthSubContainer.addSubview(self.monthSubConditions)
        self.monthSubContainer.addSubview(self.monthSubCheckBorder)
        self.monthSubCheckBorder.addSubview(self.monthSubCheckCenter)

        self.containerView.addSubview(self.yearSubContainerShadow)
        self.yearSubContainerShadow.addSubview(self.monthSubContainer)
        self.yearSubContainer.addSubview(self.trialLabel)
        self.yearSubContainer.addSubview(self.yearSubAction)
        self.yearSubContainer.addSubview(self.yearSubConditions)
        self.yearSubContainer.addSubview(self.yearSubCheckBorder)
        self.yearSubContainer.addSubview(self.yearSubSeparator)
        self.yearSubContainer.addSubview(self.yearSubMonthLabel)
        self.yearSubContainer.addSubview(self.yearSubMonthPrice)
        self.yearSubContainer.addSubview(self.yearSubDiscount)
        self.yearSubCheckBorder.addSubview(self.yearSubCheckCenter)

        self.containerView.addSubview(self.conditionsLabel)
        self.containerView.addSubview(self.actionButton)
        self.containerView.addSubview(self.termsLabel)

        view.addSubview(self.conditionContainer)
        self.conditionContainer.addSubview(self.conditionsButton)
        self.conditionContainer.addSubview(self.termsButton)
    }

    func createElements() {
        self.backgroundImage.snp.makeConstraints { make in
            make.left.right.equalTo(0.0)
            make.top.bottom.equalTo(0.0)
        }

        self.upperView.snp.makeConstraints { make in
            make.left.right.equalTo(0.0)
            make.height.equalTo(self.barHeight * 1.5)
        }

        view.backgroundColor = UIColor(red: 240.0 / 255.0, green: 240.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)

        self.closeButton.frame = CGRect(x: self.inset, y: self.inset, width: self.barHeight - 2 * self.inset, height: self.barHeight - 2 * self.inset)

        self.closeButton.snp.makeConstraints { make in
            make.left.equalTo(self.inset)
            make.centerY.equalTo(self.upperView.snp.centerY)
        }

        self.restoreButton.snp.makeConstraints { make in
            make.right.equalTo(-self.inset)
            make.centerY.equalTo(self.upperView.snp.centerY)
        }

        self.conditionContainer.frame = CGRect(x: 0.0, y: view.frame.height - self.conditionBarHeight, width: view.frame.width, height: self.conditionBarHeight)

        let preferredLanguage = NSLocale.preferredLanguages[0]

        if preferredLanguage.starts(with: "ru") {
            print("russian")
            self.conditionsButton.snp.makeConstraints { make in
                make.leading.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
                make.width.equalTo(130)
            }

            self.termsButton.snp.makeConstraints { make in
                make.trailing.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
                make.width.equalTo(190)
            }
        } else {
            print("english")

            self.conditionsButton.snp.makeConstraints { make in
                make.leading.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
                make.width.equalTo(100)
            }

            self.termsButton.snp.makeConstraints { make in
                make.trailing.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
                make.width.equalTo(130)
            }
        }

        self.containerView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.bottom.equalTo(self.conditionContainer.snp.top).inset(-20)
        }

        var currentPosition: CGFloat = self.inset / 2.0

        self.premiumTitleLabel.snp.makeConstraints { make in
            // make.leading.trailing.equalTo(16.0)
            make.centerX.equalToSuperview()
            make.width.equalTo(360)
            make.top.equalTo(self.containerView.snp.top).offset(inset / 2.0)
        }

        currentPosition = currentPosition + self.premiumTitleLabel.frame.height + self.inset / 2.0

        self.advantageOneLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(16.0)
            make.top.equalTo(self.premiumTitleLabel.snp.bottom).offset(inset / 2.0)
        }

        currentPosition = currentPosition + self.advantageOneLabel.frame.height + self.inset / 2.0

        self.advantageTwoLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(16.0)
            make.top.equalTo(self.advantageOneLabel.snp.bottom).offset(inset / 2.0)
        }

        currentPosition = currentPosition + self.advantageTwoLabel.frame.height + self.inset / 2.0

        self.advantageThreeLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.top.equalTo(self.advantageTwoLabel.snp.bottom).offset(inset / 2.0)
        }

        currentPosition = currentPosition + self.advantageThreeLabel.frame.height + self.inset

        self.monthSubContainerShadow.snp.makeConstraints { make in
            make.width.equalTo(self.containerView).offset(-32.0)
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.height.equalTo(self.subscriptionContainerHeight)
            make.top.equalTo(self.advantageThreeLabel.snp.bottom).offset(inset / 2.0)
        }

        self.yearSubContainer.snp.makeConstraints { make in
            make.leading.equalTo(0.0)
            make.trailing.equalTo(0.0)
            make.width.equalTo(self.monthSubContainerShadow.snp.width)
            make.height.equalTo(self.monthSubContainerShadow.snp.height)
        }

        self.monthSubContainer.layer.cornerRadius = self.monthSubContainerShadow.layer.cornerRadius

        if !self.isYearSubChosen {
            self.monthSubContainer.layer.borderWidth = 2.0
            self.monthSubContainer.layer.borderColor = Colors.mainColor.cgColor
        } else {
            self.monthSubContainer.layer.borderWidth = 0.0
            self.monthSubContainer.layer.borderColor = UIColor.clear.cgColor
        }

        self.monthSubCheckBorder.snp.makeConstraints { make in
            make.leading.equalTo(inset)
            make.centerY.equalTo(monthSubContainer.snp.centerY)
            make.width.equalTo(self.subscriptionContainerHeight / 5.0)
            make.height.equalTo(self.subscriptionContainerHeight / 5.0)
        }
        self.monthSubCheckBorder.layer.cornerRadius = self.subscriptionContainerHeight / 5.0 / 2.0

        self.monthSubCheckCenter.snp.makeConstraints { make in
            make.center.equalTo(monthSubCheckBorder.snp.center)
            make.width.equalTo(0.6 * self.subscriptionContainerHeight / 5.0)
            make.height.equalTo(0.6 * self.subscriptionContainerHeight / 5.0)
        }

        if !self.isYearSubChosen {
            self.monthSubCheckCenter.backgroundColor = Colors.mainColor
        } else {
            self.monthSubCheckCenter.backgroundColor = .clear
        }
        self.monthSubCheckCenter.layer.cornerRadius = 0.6 * self.subscriptionContainerHeight / 5.0 / 2.0

        self.monthSubAction.snp.makeConstraints { make in
            make.left.equalTo(self.monthSubCheckBorder.snp.right).offset(inset)
            make.trailing.equalTo(0.0)
            make.top.equalTo(self.subscriptionContainerHeight / 5.0)
        }

        self.monthSubConditions.snp.makeConstraints { make in
            make.left.equalTo(self.monthSubCheckBorder.snp.right).offset(inset)
            make.trailing.equalTo(0.0)
            make.top.equalTo(self.monthSubAction.snp.bottom).offset(inset / 2.0)
        }

        currentPosition = currentPosition + self.monthSubContainerShadow.frame.height + self.inset / 2.0

        self.yearSubContainerShadow.snp.makeConstraints { make in
            make.width.equalTo(self.containerView).offset(-32.0)
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.height.equalTo(self.subscriptionContainerHeight)
            make.top.equalTo(self.monthSubContainerShadow.snp.bottom).offset(inset / 2.0)
        }

        self.monthSubContainer.snp.makeConstraints { make in
            make.leading.equalTo(0.0)
            make.trailing.equalTo(0.0)
            make.width.equalTo(self.yearSubContainerShadow.snp.width)
            make.height.equalTo(self.yearSubContainerShadow.snp.height)
        }
        /*     self.subYearButton.snp.makeConstraints { make in
             make.leading.equalTo(0.0)
             make.trailing.equalTo(0.0)
             make.width.equalTo(self.yearSubContainerShadow.snp.width)
             make.height.equalTo(self.yearSubContainerShadow.snp.height)
         }   */
        self.yearSubContainer.layer.cornerRadius = self.yearSubContainerShadow.layer.cornerRadius

        if self.isYearSubChosen {
            self.yearSubContainer.layer.borderWidth = 4.0
            self.yearSubContainer.layer.borderColor = Colors.mainColor.cgColor
        } else {
            self.yearSubContainer.layer.borderWidth = 0.0
            self.yearSubContainer.layer.borderColor = UIColor.clear.cgColor
        }

        self.yearSubCheckBorder.snp.makeConstraints { make in
            make.leading.equalTo(inset)
            make.centerY.equalTo(yearSubContainer.snp.centerY)
            make.width.equalTo(self.subscriptionContainerHeight / 5.0)
            make.height.equalTo(self.subscriptionContainerHeight / 5.0)
        }
        self.yearSubCheckBorder.layer.cornerRadius = self.subscriptionContainerHeight / 5.0 / 2.0

        self.yearSubCheckCenter.snp.makeConstraints { make in
            make.center.equalTo(yearSubCheckBorder.snp.center)
            make.width.equalTo(0.6 * self.subscriptionContainerHeight / 5.0)
            make.height.equalTo(0.6 * self.subscriptionContainerHeight / 5.0)
        }
        self.yearSubCheckCenter.layer.cornerRadius = 0.6 * self.subscriptionContainerHeight / 5.0 / 2.0
        if self.isYearSubChosen {
            self.yearSubCheckCenter.backgroundColor = Colors.mainColor
        } else {
            self.yearSubCheckCenter.backgroundColor = UIColor.clear
        }

        self.yearSubAction.snp.makeConstraints { make in
            make.left.equalTo(self.yearSubCheckBorder.snp.right).offset(inset)
            make.top.equalTo(self.subscriptionContainerHeight / 5.0)
        }

        self.yearSubConditions.snp.makeConstraints { make in
            make.left.equalTo(self.yearSubCheckBorder.snp.right).offset(inset)
            make.top.equalTo(self.yearSubAction.snp.bottom).offset(inset / 2.0)
        }

        self.trialLabel.snp.makeConstraints { make in
            make.left.equalTo(self.yearSubCheckBorder.snp.right).offset(inset)
            make.top.equalTo(self.yearSubConditions).inset(20)
        }

        self.yearSubSeparator.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-80.0)
            make.top.equalTo(0.0)
            make.bottom.equalTo(0.0)
            make.width.equalTo(2.0)
        }
        self.yearSubSeparator.backgroundColor = UIColor.lightGray

        self.yearSubDiscount.snp.makeConstraints { make in
            make.centerX.equalTo(self.yearSubMonthLabel.snp.centerX)
            make.left.equalTo(self.yearSubSeparator.snp.right).offset(inset / 2.0)
            make.trailing.equalTo(-inset / 2.0)
            // make.top.equalTo(self.yearSubMonthLabel.snp.bottom).offset(inset / 2.0)
            make.centerY.equalToSuperview()
        }

        self.yearSubMonthPrice.snp.makeConstraints { make in
            make.left.equalTo(self.yearSubSeparator.snp.right).offset(inset / 2.0)
            make.trailing.equalTo(-inset / 2.0)
            make.centerX.equalTo(yearSubDiscount)
            make.top.equalTo(yearSubDiscount).inset(25)
        }

        self.yearSubMonthLabel.snp.makeConstraints { make in
            // make.left.equalTo(self.yearSubSeparator.snp.right).offset(inset / 2.0)
            // make.trailing.equalTo(-inset / 2.0)
            make.centerX.equalTo(yearSubMonthPrice)
            make.top.equalTo(yearSubMonthPrice).inset(10)
        }

        self.conditionsLabel.snp.makeConstraints { make in
            make.width.equalTo(self.containerView).offset(-32.0)
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.top.equalTo(self.yearSubContainerShadow.snp.bottom).offset(inset / 2.0)
        }

        self.actionButton.snp.makeConstraints { make in
            make.width.equalTo(self.containerView).offset(-32.0)
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.height.equalTo(self.actionButtonHeight)
            make.top.equalTo(self.conditionsLabel.snp.bottom).offset(inset / 2.0)
        }

        self.actionButton.layer.cornerRadius = self.actionButtonHeight / 2.0

        self.termsLabel.snp.makeConstraints { make in
            make.width.equalTo(self.containerView).offset(-32.0)
            make.centerX.equalTo(self.containerView.snp.centerX)
            make.top.equalTo(self.actionButton.snp.bottom).offset(inset / 2.0)
            make.bottom.equalTo(self.containerView.snp.bottom).offset(-inset / 2.0)
        }

        self.containerView.isUserInteractionEnabled = true

        self.activityMonitor.color = ThemesColors.mainColor
        self.activityMonitor.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        self.activityMonitor.center = self.view.center
        self.view.addSubview(self.activityMonitor)
        self.activityMonitor.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(20.0)
            make.width.equalTo(20.0)
        }
    }
}
