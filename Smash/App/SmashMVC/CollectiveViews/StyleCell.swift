import Foundation
import TinyConstraints
import UIKit

class StyleCell: UICollectionViewCell, SelfConfiguringCell {
    static let reuseIdentifier: String = "StyleCell"

    // http://avatars.adorable.io/
    fileprivate let imageApiBaseUrl = "https://api.adorable.io/avatars/285/"

    var isService: Bool = false
    var isLocked: Bool = false

    var isCellMarginIsBlack: Bool = UserDefaults.standard.bool(forKey: "isBlackThemeChoosed")

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var styleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    lazy var lockImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    lazy var highlightView: UIImageView = {
        let highlightView = UIImageView()
        highlightView.layer.cornerRadius = 8
        highlightView.clipsToBounds = true
        highlightView.contentMode = .scaleAspectFit
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            highlightView
                .backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0) // init(red: 142 / 255, green: 20 / 255, blue: 27 / 255, alpha: 1.0)
        } else {
            highlightView.backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0) // init(red: 142 / 255, green: 20 / 255, blue: 27 / 255, alpha: 1.0)
        }
        return highlightView
    }()

    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView()
        aiv.startAnimating()
        return aiv
    }()

    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 22)
        label.textColor = UIColor.black
        label.textAlignment = .center
        return label
    }()

    func highlightCell() {
        if isSelected {
            layer.borderColor = ThemesColors.mainColor.cgColor
            layer.borderWidth = 2.0
        } else {
            layer.borderColor = UIColor.clear.cgColor
            layer.borderWidth = 0.0
        }
    }

    func addNameLabel() {
        self.nameLabel.font = .boldSystemFont(ofSize: 12)
        self.nameLabel.sizeToFit()
        self.nameLabel.textAlignment = .center
        addSubview(self.nameLabel)
        self.nameLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalTo(80)
            make.height.equalTo(15)
        }
        if self.isService {
            self.nameLabel.removeFromSuperview()
        }
    }

    func lockIfNeeded() {
        if self.isLocked {
            self.lockImageView.image = UIImage(named: "locked")
            addSubview(self.lockImageView)
            self.lockImageView.snp.makeConstraints { make in
                make.width.height.equalTo(64.0)
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview()
                // make.top.equalTo(0.0)
            }
        }
    }

    func chooseBackgroundColor() {
        if self.isCellMarginIsBlack == false {
            layer.backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
            self.highlightView.backgroundColor = UIColor(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
            self.highlightView.layer
                .backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
        } else {
            layer.backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0)
            self.highlightView.backgroundColor = UIColor.darkGray
            self.highlightView.layer.backgroundColor = UIColor.darkGray.cgColor
        }

        if self.isService {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                self.styleImageView
                    .backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0) // init(red: 142 / 255, green: 20 / 255, blue: 27 / 255, alpha: 1.0)
            } else {
                self.styleImageView
                    .backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0)
            }
        }
    }

    func setupView() {
        layer.cornerRadius = 8
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            layer.backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
        } else {
            layer.backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0)
        }

        clipsToBounds = true
        addSubview(self.highlightView)
        addSubview(self.styleImageView)
        self.styleImageView.image = UIImage(systemName: "video")
        self.styleImageView.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
        self.styleImageView.snp.makeConstraints { make in
            make.width.height.equalTo(64.0)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }

        self.highlightView.snp.makeConstraints { make in
            make.width.height.equalTo(80.0)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }

    func highlited() {
        print("highlited")
        self.styleImageView.image = UIImage(named: "locked")
    }

    func configure(with style: Style) {
        self.nameLabel.text = style.styleName
    }
}
