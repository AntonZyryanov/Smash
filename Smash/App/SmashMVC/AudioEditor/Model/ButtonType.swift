import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    enum ButtonType: Int {
        case slow = 0, fast, highPitch, rain, wasp, reverb3, forest, distortion2, sea, echo, delay, vader, club, police, helicopter, owl, bonfire, coffeeshop, war,
             radiation,
             bus, jungles, reverb2, barbershop, swamp, reverb1, coop, applause, construction, watch, shower, hairdryer, hurricane, basketball, cave, horse, swings, phone,
             washingmachine,
             rockingchair
    }
}
