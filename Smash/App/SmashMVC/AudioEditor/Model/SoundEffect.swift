import Foundation
import UIKit

class SoundEffect {
    let name: String
    let image: UIImage
    let id: Int

    init(name: String, image: UIImage, id: Int) {
        self.name = name
        self.image = image
        self.id = id
    }

    static func loadSoundEffects() -> [SoundEffect] {
        let soundEffects = [
            SoundEffect(
                name: NSLocalizedString("slowdown", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "slow") ?? UIImage(),
                id: 0
            ),
            SoundEffect(
                name: NSLocalizedString("speed-up", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "fast") ?? UIImage(),
                id: 1
            ),
            SoundEffect(
                name: NSLocalizedString("helium", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "helium") ?? UIImage(),
                id: 2
            ),
            SoundEffect(
                name: NSLocalizedString("rain", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "rain") ?? UIImage(),
                id: 3
            ),
            SoundEffect(
                name: NSLocalizedString("wasp", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "wasp") ?? UIImage(),
                id: 4
            ),
            SoundEffect(
                name: NSLocalizedString("cathedral", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "cathedral") ?? UIImage(),
                id: 5
            ),
            SoundEffect(
                name: NSLocalizedString("forest", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "forest") ?? UIImage(),
                id: 6
            ),
            SoundEffect(
                name: NSLocalizedString("radio", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "radio") ?? UIImage(),
                id: 7
            ),
            SoundEffect(name: NSLocalizedString("sea", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""), image: UIImage(named: "sea") ?? UIImage(), id: 8),
            SoundEffect(
                name: NSLocalizedString("train", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "train") ?? UIImage(),
                id: 9
            ),
            SoundEffect(
                name: NSLocalizedString("mountains", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "mountains") ?? UIImage(),
                id: 10
            ),
            SoundEffect(
                name: NSLocalizedString("anonymous", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "vader") ?? UIImage(),
                id: 11
            ),
            SoundEffect(
                name: NSLocalizedString("club", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "club") ?? UIImage(),
                id: 12
            ),
            SoundEffect(
                name: NSLocalizedString("police", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "police") ?? UIImage(),
                id: 13
            ),
            SoundEffect(
                name: NSLocalizedString("helicopter", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "helicopter") ?? UIImage(),
                id: 14
            ),
            SoundEffect(name: NSLocalizedString("owl", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""), image: UIImage(named: "owl") ?? UIImage(), id: 15),
            SoundEffect(
                name: NSLocalizedString("bonfire", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "bonfire") ?? UIImage(),
                id: 16
            ),
            SoundEffect(
                name: NSLocalizedString("coffeeshop", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "coffeeshop") ?? UIImage(),
                id: 17
            ),
            SoundEffect(
                name: NSLocalizedString("battle", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "war") ?? UIImage(),
                id: 18
            ),
            SoundEffect(
                name: NSLocalizedString("radiation", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "radiation") ?? UIImage(),
                id: 19
            ),
            SoundEffect(name: NSLocalizedString("bus", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""), image: UIImage(named: "bus") ?? UIImage(), id: 20),
            SoundEffect(
                name: NSLocalizedString("jungles", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "jungles") ?? UIImage(),
                id: 21
            ),
            SoundEffect(
                name: NSLocalizedString("submarine", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "submarine") ?? UIImage(),
                id: 22
            ),
            SoundEffect(
                name: NSLocalizedString("barbershop", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "barbershop") ?? UIImage(),
                id: 23
            ),
            SoundEffect(
                name: NSLocalizedString("swamp", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "swamp") ?? UIImage(),
                id: 24
            ),
            SoundEffect(
                name: NSLocalizedString("kolizeum", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "kolizeum") ?? UIImage(),
                id: 25
            ),
            SoundEffect(
                name: NSLocalizedString("coop", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "coop") ?? UIImage(),
                id: 26
            ),
            SoundEffect(
                name: NSLocalizedString("applause", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "applause") ?? UIImage(),
                id: 27
            ),
            SoundEffect(
                name: NSLocalizedString("construction", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "construction") ?? UIImage(),
                id: 28
            ),
            SoundEffect(
                name: NSLocalizedString("watch", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "watch") ?? UIImage(),
                id: 29
            ),
            SoundEffect(
                name: NSLocalizedString("shower", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "shower") ?? UIImage(),
                id: 30
            ),
            SoundEffect(
                name: NSLocalizedString("hair dryer", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "hairdryer") ?? UIImage(),
                id: 31
            ),
            SoundEffect(
                name: NSLocalizedString("hurricane", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "hurricane") ?? UIImage(),
                id: 32
            ),
            SoundEffect(
                name: NSLocalizedString("basketball", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "basketball") ?? UIImage(),
                id: 33
            ),
            SoundEffect(
                name: NSLocalizedString("cave", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "cave") ?? UIImage(),
                id: 34
            ),
            SoundEffect(
                name: NSLocalizedString("horse", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "horse") ?? UIImage(),
                id: 35
            ),
            SoundEffect(
                name: NSLocalizedString("swings", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "swings") ?? UIImage(),
                id: 36
            ),
            SoundEffect(
                name: NSLocalizedString("phone", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "phone") ?? UIImage(),
                id: 37
            ),
            SoundEffect(
                name: NSLocalizedString("washer", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "washingmachine") ?? UIImage(),
                id: 38
            ),
            SoundEffect(
                name: NSLocalizedString("rocking chair", tableName: "effectsLocalization", bundle: .main, value: "", comment: ""),
                image: UIImage(named: "rockingchair") ?? UIImage(),
                id: 39
            ),
            SoundEffect(name: "Effects picker", image: UIImage(named: "plus") ?? UIImage(), id: 40),
            SoundEffect(name: "openEditor", image: UIImage(named: "openEditor") ?? UIImage(), id: 41),
        ]
        return soundEffects
    }
}
