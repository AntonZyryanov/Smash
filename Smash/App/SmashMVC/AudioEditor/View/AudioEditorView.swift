import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    func AudioEditorControllerSetup() {
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            view.backgroundColor = .white
        } else {
            view.backgroundColor = .gray
        }
        view.addSubview(self.videoView)
        view.addSubview(self.lowPanel)
        view.addSubview(self.stopButton)
        view.addSubview(self.saveButton)
        view.addSubview(self.backButton)
        view.addSubview(self.shareButton)
        view.addSubview(self.collectionView)
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.lowPanel.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            self.lowPanel.backgroundColor = UIColor.darkGray
        }
        self.lowPanel.layer.cornerRadius = 20
        self.lowPanel.clipsToBounds = true
        self.lowPanel.contentMode = .scaleAspectFit
        self.stopButton.layer.cornerRadius = 25.0
        self.stopButton.layer.masksToBounds = true
        self.stopButton.setTitle(NSLocalizedString("stop button", tableName: "AudioEditorControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        self.stopButton.setTitleColor(ThemesColors.mainColor, for: .normal)
        self.stopButton.titleLabel?.textAlignment = .left
        self.stopButton.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        self.saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        self.saveButton.titleLabel?.textAlignment = .center
        self.saveButton.layer.cornerRadius = 25.0
        self.saveButton.layer.masksToBounds = true

        self.saveButton.setTitle(NSLocalizedString("save button", tableName: "AudioEditorControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)

        self.saveButton.setTitleColor(ThemesColors.mainColor, for: .normal)
        self.saveButton.titleLabel?.textAlignment = .right
        self.backButton.setImage(UIImage(named: "backButton"), for: .normal)
        self.shareButton.setImage(UIImage(named: "sharedButton"), for: .normal)
        self.stopButton.addTarget(self, action: #selector(self.stopButtonPressed), for: .touchUpInside)
        self.saveButton.addTarget(self, action: #selector(self.saveVideo), for: .touchUpInside)
        self.backButton.addTarget(self, action: #selector(self.goBack), for: .touchUpInside)
        self.shareButton.addTarget(self, action: #selector(self.share), for: .touchUpInside)

        // indicator setup
        self.activityMonitor.color = ThemesColors.mainColor
        self.activityMonitor.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        self.activityMonitor.center = self.view.center
        self.lowPanel.addSubview(self.activityMonitor)

        self.collectionView.allowsMultipleSelection = false
        self.soundEffectVolumeSlider.tintColor = ThemesColors.mainColor
        self.soundEffectVolumeSlider.maximumValue = 1.00
        self.soundEffectVolumeSlider.value = 0.50
        self.soundEffectVolumeSlider.addTarget(self, action: #selector(self.changeSoundEffectVolume), for: .valueChanged)

        self.backButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(50)
            make.width.height.equalTo(24)
        }

        self.shareButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(50)
            make.width.height.equalTo(24)
        }

        if Apphud.hasActiveSubscription() == false {
            self.lowPanel.snp.makeConstraints { make in

                make.bottom.equalToSuperview().inset(-20)
                make.width.equalToSuperview()
                make.height.equalTo(238)
            }

            self.collectionView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(0)
                make.trailing.equalToSuperview().inset(0)
                make.height.equalTo(100.0)
                make.bottom.equalToSuperview().inset(50)
            }

            self.stopButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview().inset(164)
                make.height.equalTo(50.0)
                make.leading.equalToSuperview().inset(20)
            }

            self.saveButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview().inset(164)
                make.height.equalTo(50.0)
                make.trailing.equalToSuperview().inset(20)
            }

            self.videoView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(0)
                make.trailing.equalToSuperview().inset(0)
                make.top.equalToSuperview()
                make.bottom.equalTo(self.lowPanel.snp.top).offset(24.0)
            }
        } else {
            self.lowPanel.snp.makeConstraints { make in
                make.bottom.equalToSuperview().inset(-20)
                make.width.equalToSuperview()
                make.height.equalTo(218)
            }

            self.collectionView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(0)
                make.trailing.equalToSuperview().inset(0)
                make.height.equalTo(100.0)
                make.bottom.equalToSuperview()
            }

            self.stopButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview().inset(134)
                make.height.equalTo(50.0)
                make.leading.equalToSuperview().inset(20)
            }

            self.saveButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview().inset(134)
                make.height.equalTo(50.0)
                make.trailing.equalToSuperview().inset(20)
            }

            self.videoView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(0)
                make.trailing.equalToSuperview().inset(0)
                make.top.equalToSuperview()
                make.bottom.equalTo(self.lowPanel.snp.top).offset(24.0)
            }
        }

        // AVPlayer setup
        // collectionView setup

        self.activityMonitor.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.top.equalToSuperview().inset(10)
            make.height.equalTo(20.0)
            make.width.equalTo(20.0)
        }
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.videoView.backgroundColor = .white
        } else {
            self.videoView.backgroundColor = .gray
        }
    }
}
