import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

extension AudioEditorController {
    func mixTwoAudioTracks(
        firstAudioUrl: URL,
        secondAudioUrl: URL,
        audioPlayerNode: AVAudioPlayerNode,
        backgroundSoundEffectPlayerNode: AVAudioPlayerNode,
        audioEngine: AVAudioEngine,
        rate: Float? = nil,
        audioFile: AVAudioFile,
        choosedToSave: Bool,
        playSoundFuncIdCheck: String,
        onClose: ((AVAudioPlayerNode, AVAudioEngine, Float?, URL, AVAudioFile, Bool, String) -> Void)? = nil
    ) -> URL? {
        let firstAudioAsset = AVAsset(url: firstAudioUrl)
        let secondAudioAsset = AVAsset(url: secondAudioUrl)

        let mixComposition = AVMutableComposition()

        // 1 track
        let firstAudioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0
        )
        do {
            try firstAudioTrack?.insertTimeRange(
                CMTimeRangeMake(
                    start: .zero,
                    duration: firstAudioAsset.duration
                ),
                of: firstAudioAsset.tracks(withMediaType: .audio)[0],
                at: .zero
            )
        } catch {
            print("Failed to load Audio track")
        }
        // 2 track

        let secondAudioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 1
        )
        do {
            try secondAudioTrack?.insertTimeRange(
                CMTimeRangeMake(
                    start: .zero,
                    duration: firstAudioAsset.duration
                ),
                of: secondAudioAsset.tracks(withMediaType: .audio)[0],
                at: .zero
            )
        } catch {
            print("Failed to load Audio track")
        }
        // Sound Effect volume
        let audioMix = AVMutableAudioMix()
        let trackVolumeMix = AVMutableAudioMixInputParameters(track: secondAudioTrack)
        trackVolumeMix.trackID = secondAudioTrack!.trackID
        trackVolumeMix.setVolume(self.soundEffectVolume, at: CMTime.zero)
        audioMix.inputParameters = [trackVolumeMix]

        let tmpDirectory = URL(fileURLWithPath: NSTemporaryDirectory()) // extra
        // name generating
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var date = dateFormatter.string(from: Date())
        let notNeededCharacters: Set<Character> = [",", ":", " "]
        date.removeAll(where: { notNeededCharacters.contains($0) })
        print(date)
        let randomNumber = Int.random(in: 0 ..< 1000)
        let effectURLString = tmpDirectory.absoluteString + date + String(randomNumber) + ".m4a" // ex
        let url = URL(string: effectURLString)
        // exporting
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetAppleM4A)
        assetExport?.outputFileType = AVFileType.m4a
        assetExport?.outputURL = url
        assetExport?.audioMix = audioMix
        assetExport?.exportAsynchronously(completionHandler: {
            switch assetExport!.status {
            case AVAssetExportSession.Status.failed:
                print("failed \(assetExport?.error?.localizedDescription ?? "")")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport?.error?.localizedDescription ?? "")")
            case AVAssetExportSession.Status.unknown:
                print("unknown\(assetExport?.error?.localizedDescription ?? "")")
            case AVAssetExportSession.Status.waiting:
                print("waiting\(assetExport?.error?.localizedDescription ?? "")")
            case AVAssetExportSession.Status.exporting:
                print("exporting\(assetExport?.error?.localizedDescription ?? "")")
            default:
                self.backgroundSoundEffectChosen = false
                onClose!(audioPlayerNode, audioEngine, rate, url!, audioFile, choosedToSave, playSoundFuncIdCheck)
            }
        })
        return url
    }
}
