import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

extension AudioEditorController {
    @objc func stopAudio() {
        if self.playsoundFunctionIdChecker == self.currentPlaySoundFunctionId {
            if self.audioPlayerNode != nil {
                self.audioPlayerNode.stop()
            }

            if self.backgroundSoundEffectPlayerNode != nil {
                self.backgroundSoundEffectPlayerNode.stop()
            }

            if isSoundEffectSliderOn == true {
                soundEffectVolumeSlider.removeFromSuperview()
            }

            self.stopTimer?.invalidate()
            self.stopTimer = nil

            self.configureUI(.notPlaying)

            if self.audioEngine != nil {
                self.audioEngine.stop()
                self.audioEngine.reset()
            }
        } else {
            print("stopAudio func blocked")
        }
    }

    @objc func stopAudioWithoutBlocking() {
        if self.audioPlayerNode != nil {
            self.audioPlayerNode.stop()
        }

        if self.backgroundSoundEffectPlayerNode != nil {
            self.backgroundSoundEffectPlayerNode.stop()
        }

        if isSoundEffectSliderOn == true {
            soundEffectVolumeSlider.removeFromSuperview()
        }

        self.stopTimer?.invalidate()
        self.stopTimer = nil

        self.configureUI(.notPlaying)

        if self.audioEngine != nil {
            self.audioEngine.stop()
            self.audioEngine.reset()
        }
    }
}
