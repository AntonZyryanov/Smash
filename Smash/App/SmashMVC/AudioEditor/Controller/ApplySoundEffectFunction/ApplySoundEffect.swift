import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

// MARK: - PlaySoundsViewController: AVAudioPlayerDelegate

extension AudioEditorController: AVAudioPlayerDelegate {
    // MARK: PlayingState (raw values correspond to sender tags)

    enum PlayingState { case playing, notPlaying }

    // MARK: Audio Functions

    func setupAudio() {
        // initialize (recording) audio file
        do {
            audioFile = try AVAudioFile(forReading: recordedAudioURL as URL)
        } catch {
            showAlert(Alerts.AudioFileError, message: String(describing: error))
        }
    }

    // MARK: Play Sound

    func applySoundEffect(
        rate: Float? = nil,
        pitch: Float? = nil,
        echo: Bool = false,
        reverb: Bool = false,
        vader: Bool = false,
        forest: Bool = false,
        distortion2: Bool = false,
        delay: Bool = false,
        sea: Bool = false,
        reverb2: Bool = false,
        reverb3: Bool = false,
        train: Bool = false,
        club: Bool = false,
        police: Bool = false,
        helicopter: Bool = false,
        owl: Bool = false,
        bonfire: Bool = false,
        coffeeshop: Bool = false,
        war: Bool = false,
        radiation: Bool = false,
        bus: Bool = false,
        jungles: Bool = false,
        swamp: Bool = false,
        barbershop: Bool = false,
        wasp: Bool = false,
        rain: Bool = false,
        coop: Bool = false,
        applause: Bool = false,
        construction: Bool = false,
        watch: Bool = false,
        shower: Bool = false,
        hairdryer: Bool = false,
        hurricane: Bool = false,
        basketball: Bool = false,
        cave: Bool = false,
        horse: Bool = false,
        swings: Bool = false,
        phone: Bool = false,
        washingmachine: Bool = false,
        rockingchair: Bool = false,
        isBackgroundSoundEffect: Bool = false,
        onClose: (() -> Void)? = nil,
        choosedToSave: Bool = false,
        playSoundFunctionIdCheck: String
    ) {
        if isBackgroundSoundEffect == true {
            self.backgroundSoundEffectChosen = true
            // self.videoView.removeFromSuperview()
        }
        audioEngine = AVAudioEngine()

        // node for playing audio
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)

        // second player node for editing volume of background sound effect

        backgroundSoundEffectPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(backgroundSoundEffectPlayerNode)

        // node for adjusting rate/pitch
        let changeRatePitchNode = AVAudioUnitTimePitch()
        if let pitch = pitch {
            changeRatePitchNode.pitch = pitch
        }
        if let rate = rate {
            changeRatePitchNode.rate = rate
        }
        audioEngine.attach(changeRatePitchNode)

        // node for echo
        let echoNode = AVAudioUnitEQ(numberOfBands: 1)

        audioEngine.attach(echoNode)

        let filterParams = echoNode.bands[0] as AVAudioUnitEQFilterParameters
        // let filterParams2 = echoNode.bands[1] as AVAudioUnitEQFilterParameters

        filterParams.filterType = .bandPass
        filterParams.frequency = 1000
        filterParams.bandwidth = 0.10
        filterParams.gain = 300.0
        filterParams.bypass = false

        let distortionNode = AVAudioUnitDistortion()
        audioEngine.attach(distortionNode)
        distortionNode.loadFactoryPreset(.drumsLoFi) // was .multiBrokenSpeaker

        let distortionNode2 = AVAudioUnitDistortion()
        audioEngine.attach(distortionNode2)
        distortionNode2.loadFactoryPreset(.multiCellphoneConcert)
        distortionNode2.wetDryMix = 15
        distortionNode2.preGain = -5.0

        let delayNode = AVAudioUnitDelay()
        audioEngine.attach(delayNode)
        delayNode.delayTime = 0.20
        delayNode.wetDryMix = 60

        let reverbNode = AVAudioUnitReverb()
        if vader {
            // node for reverb
            reverbNode.loadFactoryPreset(.mediumHall) // az
            reverbNode.wetDryMix = 16 // az
            audioEngine.attach(reverbNode)
        } else {
            // node for reverb
            reverbNode.loadFactoryPreset(.cathedral) // az
            reverbNode.wetDryMix = 50 // az
            audioEngine.attach(reverbNode)
        }

        let reverbNode2 = AVAudioUnitReverb()
        reverbNode2.loadFactoryPreset(.largeChamber)
        reverbNode2.wetDryMix = 70
        audioEngine.attach(reverbNode2)

        let reverbNode3 = AVAudioUnitReverb()
        reverbNode3.loadFactoryPreset(.largeHall2)
        reverbNode3.wetDryMix = 50
        audioEngine.attach(reverbNode3)

        // node for compressor

        // connect nodes
        if echo == true, reverb == true {
            self.connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, reverbNode, audioEngine.mainMixerNode)
        } else if echo == true {
            self.connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, audioEngine.mainMixerNode)
        } else if reverb == true {
            self.connectAudioNodes(audioPlayerNode, changeRatePitchNode, reverbNode, audioEngine.mainMixerNode)
        } else if distortion2 == true {
            self.connectAudioNodes(audioPlayerNode, distortionNode2, audioEngine.mainMixerNode)
        } else if delay == true {
            self.connectAudioNodes(audioPlayerNode, delayNode, audioEngine.mainMixerNode)
        } else if reverb2 == true {
            self.connectAudioNodes(audioPlayerNode, reverbNode2, audioEngine.mainMixerNode)
        } else if reverb3 == true {
            // self.connectAudioNodes(audioPlayerNode, reverbNode3, audioEngine.outputNode)
            self.connectAudioNodes(audioPlayerNode, reverbNode3, audioEngine.mainMixerNode)
        } else {
            self.connectAudioNodes(audioPlayerNode, changeRatePitchNode, audioEngine.mainMixerNode)
        }
        self.connectAudioNodes(backgroundSoundEffectPlayerNode, audioEngine.mainMixerNode)
        print("PlaySound function working...")
        // schedule to play and start the engine!
        audioPlayerNode.stop()
        backgroundSoundEffectPlayerNode.stop()
        // new

        // MARK: - //- Background audio effects

        let backgroundSoundEffects = [
            "forest": forest,
            "sea": sea,
            "club": club,
            "police": police,
            "helicopter": helicopter,
            "owl": owl,
            "bonfire": bonfire,
            "coffeeshop": coffeeshop,
            "war": war,
            "radiation": radiation,
            "bus": bus,
            "jungles": jungles,
            "wasp": wasp,
            "barbershop": barbershop,
            "swamp": swamp,
            "rain": rain,
            "coop": coop,
            "applause": applause,
            "construction": construction,
            "watch": watch,
            "shower": shower,
            "hairdryer": hairdryer,
            "hurricane": hurricane,
            "basketball": basketball,
            "cave": cave,
            "train": train,
            "horse": horse,
            "phone": phone,
            "washingmachine": washingmachine,
            "rockingchair": rockingchair,
            "swings": swings,
        ]

        loadBackgroundSoundEffect(effects: backgroundSoundEffects, rate: rate ?? 1.00, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
        // experiment
        if isBackgroundSoundEffect == false {
            audioPlayerNode.scheduleFile(audioFile, at: nil) {
                var delayInSeconds: Double = 0

                if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
                    if let rate = rate {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                    } else {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                    }
                }

                // new
                // schedule a stop timer for when audio finishes playing
                print("Timer tries to work")
                print(playSoundFunctionIdCheck == self.currentPlaySoundFunctionId)
                if playSoundFunctionIdCheck == self.currentPlaySoundFunctionId {
                    self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(AudioEditorController.stopAudio), userInfo: nil, repeats: false)
                    RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
                    self.playsoundFunctionIdChecker = playSoundFunctionIdCheck
                }
            }

            let tmpDirectory = URL(fileURLWithPath: NSTemporaryDirectory()) // extra
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .short
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            var date = dateFormatter.string(from: Date())
            let notNeededCharacters: Set<Character> = [",", ":", " "]
            date.removeAll(where: { notNeededCharacters.contains($0) })
            print(date)
            let randomNumber = Int.random(in: 0 ..< 1000)
            let processedAudioURLString = tmpDirectory.absoluteString + date + String(randomNumber) + ".WAV" // ex
            let processedAudioURL = URL(string: processedAudioURLString)
            chosenEffectAudioURL = processedAudioURL

            if choosedToSave == true {
                DispatchQueue.main.async(flags: .barrier) { [self] in
                    do {
                        // var newAudio = try AVAudioFile(forWriting: processedAudioURL!, settings: [:])
                        let newAudio = try AVAudioFile(
                            forWriting: processedAudioURL!,
                            settings: self.audioFile.fileFormat.settings
                        ) //  audioFile.processingFormat.commonFormat
                        // calculating time
                        // Now install a Tap on the output bus to "record" the transformed file on a our newAudio file.
                        print(self.audioFile.length)
                        // some new shit

                        do {
                            // The maximum number of frames the engine renders in any single render call.
                            let maxFrames: AVAudioFrameCount = 8096 // 4096
                            try self.audioEngine.enableManualRenderingMode(
                                .offline,
                                format: self.audioFile.processingFormat,
                                maximumFrameCount: maxFrames
                            )
                        } catch {
                            fatalError("Enabling manual rendering mode failed: \(error).")
                        }

                        let buffer = AVAudioPCMBuffer(
                            pcmFormat: self.audioEngine.manualRenderingFormat,
                            frameCapacity: self.audioEngine.manualRenderingMaximumFrameCount
                        )!
                        print(self.audioEngine.manualRenderingMaximumFrameCount)
                        // audioPlayerNode.play()
                        do {
                            try self.audioEngine.start()
                            self.audioPlayerNode.play()
                        } catch {
                            self.showAlert(Alerts.AudioEngineError, message: String(describing: error))
                            return
                        }

                        var renderingTime = self.audioFile.length

                        if chosenEffectIndicator == 1 {
                            renderingTime = self.audioFile.length + self.audioFile.length
                        }

                        while self.audioEngine.manualRenderingSampleTime < renderingTime { // bug check
                            do {
                                let frameCount = renderingTime - self.audioEngine.manualRenderingSampleTime
                                let framesToRender = min(AVAudioFrameCount(frameCount), buffer.frameCapacity)

                                print(framesToRender)
                                print(self.audioFile.length)
                                print(frameCount)

                                let status = try self.audioEngine.renderOffline(framesToRender, to: buffer)

                                switch status {
                                case .success:
                                    print("writing processed audio file:")
                                    print(newAudio.length)
                                    // The data rendered successfully. Write it to the output file.
                                    try newAudio.write(from: buffer)
                                case .insufficientDataFromInputNode:
                                    // Applicable only when using the input node as one of the sources.
                                    break

                                case .cannotDoInCurrentContext:
                                    // The engine couldn't render in the current render call.
                                    // Retry in the next iteration.
                                    break

                                case .error:
                                    // An error occurred while rendering the audio.
                                    fatalError("The manual rendering failed.")
                                @unknown default:
                                    break
                                }
                            } catch {
                                fatalError("The manual rendering failed: \(error).")
                            }
                        }
                    } catch {
                        print("Audio not processed")
                    }
                    self.mixAudioWithVideo(videoUrl: self.recordedVideoURL, audioUrl: self.chosenEffectAudioURL!)
                    self.activityIndicatorView.stopAnimating()
                    self.saveButton.isEnabled = true
                    //   DispatchQueue.main.async(flags: .ba)
                }

            } else {
                do {
                    try audioEngine.start()
                } catch {
                    showAlert(Alerts.AudioEngineError, message: String(describing: error))
                    return
                }
                audioPlayerNode.play()
            }
        }
    }

    // MARK: Connect List of Audio Nodes

    func connectAudioNodes(_ nodes: AVAudioNode...) {
        for x in 0 ..< nodes.count - 1 {
            audioEngine.connect(nodes[x], to: nodes[x + 1], format: audioFile.processingFormat)
        }
    }

    // MARK: UI Functions

    func configureUI(_ playState: PlayingState) {
        switch playState {
        case .playing:
            stopButton.isEnabled = true
        case .notPlaying:
            stopButton.isEnabled = false
        }
    }
}
