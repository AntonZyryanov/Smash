import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

extension AudioEditorController {
    func loadBackgroundSoundEffect(effects: [String: Bool], rate: Float, choosedToSave: Bool, playSoundFunctionIdCheck: String) {
        for effect in effects {
            if effect.value == true {
                self.backgroundSoundEffect(
                    effectName: effect.key,
                    rate: rate,
                    audioPlayerNode: audioPlayerNode,
                    backgroundSoundEffectPlayerNode: backgroundSoundEffectPlayerNode,
                    audioEngine: audioEngine,
                    audioFile: audioFile,
                    choosedToSave: choosedToSave,
                    playSoundFuncIdCheck: playSoundFunctionIdCheck
                )
            }
        }
    }

    func backgroundSoundEffect(
        effectName: String,
        rate: Float? = nil,
        audioPlayerNode: AVAudioPlayerNode,
        backgroundSoundEffectPlayerNode: AVAudioPlayerNode,
        audioEngine: AVAudioEngine,
        audioFile: AVAudioFile,
        choosedToSave: Bool,
        playSoundFuncIdCheck: String
    ) {
        // Perform task

        let AudioEffecturlPath = Bundle.main.path(forResource: effectName, ofType: "mp3")!
        let audioEffectURL = URL(fileURLWithPath: AudioEffecturlPath)
        if choosedToSave == true {
            let effectURL = self.mixTwoAudioTracks(
                firstAudioUrl: self.recordedAudioURL,
                secondAudioUrl: audioEffectURL,
                audioPlayerNode: audioPlayerNode,
                backgroundSoundEffectPlayerNode: backgroundSoundEffectPlayerNode,
                audioEngine: audioEngine,
                rate: rate,
                audioFile: audioFile,
                choosedToSave: choosedToSave,
                playSoundFuncIdCheck: playSoundFuncIdCheck,
                onClose: self
                    .backgroundSoundEffectProcessings(audioPlayerNode:audioEngine:rate:effectURL:audioFile:choosedToSave:playSoundFuncIdCheck:)
            )
        } else {
            // creating AVAudioFile with background sound effect
            do {
                soundEffectAudio = try AVAudioFile(forReading: audioEffectURL as URL)
            } catch {
                showAlert(Alerts.AudioFileError, message: String(describing: error))
            }
            // adding audio tracks to their players
            audioPlayerNode.scheduleFile(audioFile, at: nil) {
                var delayInSeconds: Double = 0

                if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
                    if let rate = rate {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                    } else {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                    }
                }

                // new
                // schedule a stop timer for when audio finishes playing
                print(playSoundFuncIdCheck == self.currentPlaySoundFunctionId)
                if playSoundFuncIdCheck == self.currentPlaySoundFunctionId {
                    self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(AudioEditorController.stopAudio), userInfo: nil, repeats: false)
                    RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
                    self.playsoundFunctionIdChecker = playSoundFuncIdCheck
                }
            }

            backgroundSoundEffectPlayerNode.scheduleFile(self.soundEffectAudio, at: nil) {
                var delayInSeconds: Double = 0

                if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
                    if let rate = rate {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                    } else {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                    }
                }
            }

            do {
                try audioEngine.start()
            } catch {
                showAlert(Alerts.AudioEngineError, message: String(describing: error))
                return
            }
            self.isSoundEffectSliderOn = true
            self.view.addSubview(self.soundEffectVolumeSlider)
            if Apphud.hasActiveSubscription() == true {
                self.soundEffectVolumeSlider.snp.makeConstraints { make in
                    make.width.equalTo(160)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview().inset(110)
                    make.height.equalTo(50)
                }
            } else {
                self.soundEffectVolumeSlider.snp.makeConstraints { make in
                    make.width.equalTo(160)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview().inset(140)
                    make.height.equalTo(50)
                }
            }

            backgroundSoundEffectPlayerNode.play()
            backgroundSoundEffectPlayerNode.volume = self.soundEffectVolume
            audioPlayerNode.play()
            self.backgroundSoundEffectChosen = false
        }
    }

    func backgroundSoundEffectProcessings(
        audioPlayerNode: AVAudioPlayerNode,
        audioEngine: AVAudioEngine,
        rate: Float? = nil,
        effectURL: URL,
        audioFile: AVAudioFile,
        choosedToSave: Bool,
        playSoundFuncIdCheck: String
    ) {
        do {
            //   let trainSound = try AVAudioFile(forReading: trainEffectURL as URL)
            let effectSound = try AVAudioFile(forReading: effectURL)
            audioPlayerNode.scheduleFile(effectSound, at: nil) {
                var delayInSeconds: Double = 0

                if let lastRenderTime = audioPlayerNode.lastRenderTime, let playerTime = audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
                    if let rate = rate {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                    } else {
                        delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                    }
                }

                // schedule a stop timer for when audio finishes playing
                print(playSoundFuncIdCheck == self.currentPlaySoundFunctionId)
                if playSoundFuncIdCheck == self.currentPlaySoundFunctionId {
                    self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(AudioEditorController.stopAudio), userInfo: nil, repeats: false)
                    RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
                    self.playsoundFunctionIdChecker = playSoundFuncIdCheck
                }
            }

        } catch {
            self.showAlert(Alerts.AudioFileError, message: String(describing: error))
        }
        self.chosenEffectAudioURL = effectURL
        //   self.chosenEffect = "Effect chosen"

        // part 2 (playsound function part)

        audioPlayerNode.scheduleFile(audioFile, at: nil) {
            var delayInSeconds: Double = 0

            if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
                if let rate = rate {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                } else {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                }
            }

            // schedule a stop timer for when audio finishes playing
            if playSoundFuncIdCheck == self.currentPlaySoundFunctionId {
                self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(AudioEditorController.stopAudio), userInfo: nil, repeats: false)
                RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
                self.playsoundFunctionIdChecker = playSoundFuncIdCheck
            }
        }

        // play the recording!
        //    audioPlayerNode.play()
        let tmpDirectory = URL(fileURLWithPath: NSTemporaryDirectory()) // extra
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var date = dateFormatter.string(from: Date())
        let notNeededCharacters: Set<Character> = [",", ":", " "]
        date.removeAll(where: { notNeededCharacters.contains($0) })
        print(date)
        let randomNumber = Int.random(in: 0 ..< 1000)
        let processedAudioURLString = tmpDirectory.absoluteString + date + String(randomNumber) + ".WAV" // ex
        let processedAudioURL = URL(string: processedAudioURLString)
        chosenEffectAudioURL = processedAudioURL
        // new

        if choosedToSave == true {
            DispatchQueue.main.async(flags: .barrier) { [self] in
                do {
                    // var newAudio = try AVAudioFile(forWriting: processedAudioURL!, settings: [:])
                    var newAudio = try AVAudioFile(
                        forWriting: processedAudioURL!,
                        settings: self.audioFile.fileFormat.settings
                    ) //  audioFile.processingFormat.commonFormat
                    // calculating time
                    // Now install a Tap on the output bus to "record" the transformed file on a our newAudio file.
                    print(self.audioFile.length)
                    // some new shit

                    do {
                        // The maximum number of frames the engine renders in any single render call.
                        let maxFrames: AVAudioFrameCount = 8096 // 4096
                        try audioEngine.enableManualRenderingMode(
                            .offline,
                            format: audioFile.processingFormat,
                            maximumFrameCount: maxFrames
                        )
                    } catch {
                        fatalError("Enabling manual rendering mode failed: \(error).")
                    }

                    let buffer = AVAudioPCMBuffer(
                        pcmFormat: self.audioEngine.manualRenderingFormat,
                        frameCapacity: self.audioEngine.manualRenderingMaximumFrameCount
                    )!
                    print(self.audioEngine.manualRenderingMaximumFrameCount)
                    // audioPlayerNode.play()
                    do {
                        try audioEngine.start()
                        audioPlayerNode.play()
                    } catch {
                        showAlert(Alerts.AudioEngineError, message: String(describing: error))
                        return
                    }

                    while self.audioEngine.manualRenderingSampleTime < self.audioFile.length {
                        do {
                            let frameCount = self.audioFile.length - self.audioEngine.manualRenderingSampleTime
                            let framesToRender = min(AVAudioFrameCount(frameCount), buffer.frameCapacity)

                            print(framesToRender)
                            print(self.audioFile.length)
                            print(frameCount)

                            let status = try self.audioEngine.renderOffline(framesToRender, to: buffer)

                            switch status {
                            case .success:
                                // print("buffer size:")
                                // print(buffer.frameLength)
                                print("writing processed audio file:")
                                print(newAudio.length)
                                // The data rendered successfully. Write it to the output file.
                                try newAudio.write(from: buffer)

                            case .insufficientDataFromInputNode:
                                // Applicable only when using the input node as one of the sources.
                                break

                            case .cannotDoInCurrentContext:
                                // The engine couldn't render in the current render call.
                                // Retry in the next iteration.
                                break

                            case .error:
                                // An error occurred while rendering the audio.
                                fatalError("The manual rendering failed.")
                            @unknown default:
                                break
                            }
                        } catch {
                            fatalError("The manual rendering failed: \(error).")
                        }
                    }

                    // Stop the player node and engine.
                    // The output buffer to which the engine renders the processed data.

                } catch {
                    print("Audio not processed")
                }

                self.mixAudioWithVideo(videoUrl: self.recordedVideoURL, audioUrl: self.chosenEffectAudioURL!)
                self.activityIndicatorView.stopAnimating()
                self.saveButton.isEnabled = true
            }

        } else {
            do {
                try audioEngine.start()
            } catch {
                showAlert(Alerts.AudioEngineError, message: String(describing: error))
                return
            }
            audioPlayerNode.play()
        }
    }
}
