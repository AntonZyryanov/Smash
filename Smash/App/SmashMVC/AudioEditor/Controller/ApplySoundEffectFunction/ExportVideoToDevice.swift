import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

extension AudioEditorController {
    func exportVideoToDevice(_ session: AVAssetExportSession) {
        // 1
        print(session.status)
        videoAsset = nil
        audioAsset = nil
        print(session.status == AVAssetExportSession.Status.completed)
        // 2
        guard
            session.status == AVAssetExportSession.Status.completed,
            let outputURL = session.outputURL
        else {
            return
        }

        print(outputURL)
        // 3
        let saveVideoToPhotos = {
            self.player.pause()
            // 4
            let changes: () -> Void = {
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL)
            }
            PHPhotoLibrary.shared().performChanges(changes) { saved, error in
                DispatchQueue.main.async {
                    let success = saved && (error == nil)
                    let title = success ? "Success" : "Error"
                    let message = success ? "Video saved" : "Failed to save video"

                    let alert = UIAlertController(
                        title: title,
                        message: message,
                        preferredStyle: .alert
                    )
                    alert.addAction(
                        UIAlertAction(
                            title: "OK",
                            style: UIAlertAction.Style.cancel
                        ) { _ in
                            if self.isVideoShared == true {
                                if self.chosenEffect == "none" {
                                    self.savedVideoURL = self.recordedVideoURL
                                }

                                self.activityViewController = UIActivityViewController(activityItems: [self.savedVideoURL ?? URL(fileURLWithPath: "")], applicationActivities: nil)
                                if Apphud.hasActiveSubscription() == false {
                                    let request = GADRequest()
                                    GADInterstitialAd.load(
                                        withAdUnitID: "ca-app-pub-1329450099126170/9317512543",
                                        request: request,
                                        completionHandler: { [self] ad, error in
                                            if let error = error {
                                                print("ERROR")
                                                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                                return
                                            }
                                            self.interstitial = ad
                                            self.interstitial?.fullScreenContentDelegate = self
                                            interstitial!.present(fromRootViewController: self)
                                        }
                                    )
                                } else {
                                    self.present(self.activityViewController!, animated: true, completion: nil)
                                    self.isVideoShared = false
                                }

                                //  self.present(self.activityViewController!, animated: true, completion: nil)

                            } else {
                                if Apphud.hasActiveSubscription() == true {
                                } else {
                                    if self.whatToShowAfterVideoSaving != 3 {
                                        self.whatToShowAfterVideoSaving += 1
                                    }
                                    self.activityIndicatorView.stopAnimating()
                                    self.saveButton.isEnabled = true

                                    let request = GADRequest()
                                    GADInterstitialAd.load(
                                        withAdUnitID: "ca-app-pub-1329450099126170/9317512543",
                                        request: request,
                                        completionHandler: { [self] ad, error in
                                            if let error = error {
                                                print("ERROR")
                                                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                                return
                                            }
                                            self.interstitial = ad
                                            self.interstitial?.fullScreenContentDelegate = self
                                            interstitial!.present(fromRootViewController: self)
                                        }
                                    )
                                }
                            }
                        }
                    )

                    self.videoIsExporting = false
                    SmashAnalytics.analyticsEventWithProperties(event: SmashAnalytics.Events.saveButtonPressed.rawValue, properties: ["effect name": self.chosenEffect])

                    self.present(alert, animated: true) {}
                }
            }
        }

        // 5
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    saveVideoToPhotos()
                }
            }
        } else {
            saveVideoToPhotos()
        }
    }
}
