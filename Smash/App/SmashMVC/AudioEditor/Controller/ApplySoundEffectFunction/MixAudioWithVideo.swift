import ApphudSDK
import AssetsLibrary
import AudioToolbox
import AVFoundation
import AVKit
import GoogleMobileAds
import MediaPlayer
import MobileCoreServices
import Photos
import StoreKit
import UIKit

extension AudioEditorController {
    func mixAudioWithVideo(videoUrl: URL, audioUrl: URL) {
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction = AVMutableVideoCompositionInstruction()

        // start merge

        let aVideoAsset = AVAsset(url: videoUrl)
        let aAudioAsset = AVAsset(url: audioUrl)

        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)

        let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]

        do {
            try mutableCompositionVideoTrack[0]
                .insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)

            if self.chosenEffect != "none" {
                if chosenEffectIndicator == 1 {
                    mutableCompositionVideoTrack[0]
                        .scaleTimeRange(
                            CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration),
                            toDuration: CMTimeMake(value: Int64(2 * savingTime * 10000), timescale: 10000)
                        )
                }

                if chosenEffectIndicator == 2 {
                    mutableCompositionVideoTrack[0]
                        .scaleTimeRange(
                            CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration),
                            toDuration: CMTimeMake(value: Int64(0.667 * savingTime * 10000), timescale: 10000)
                        )
                }
            }

            if self.chosenEffect == "none" {
                try mutableCompositionAudioTrack[0]
                    .insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
            } else {
                if chosenEffectIndicator == 1 {
                    try mutableCompositionAudioTrack[0]
                        .insertTimeRange(
                            CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMake(value: Int64(2.000 * savingTime * 10000), timescale: 10000)),
                            of: aAudioAssetTrack,
                            at: CMTime.zero
                        )
                } else {
                    if chosenEffectIndicator == 2 {
                        try mutableCompositionAudioTrack[0]
                            .insertTimeRange(
                                CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMake(value: Int64(0.667 * savingTime * 10000), timescale: 10000)),
                                of: aAudioAssetTrack,
                                at: CMTime.zero
                            )
                    } else {
                        try mutableCompositionAudioTrack[0]
                            .insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                    }
                }
            }

        } catch {}

        totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration)

        let mutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)

        mutableVideoComposition.renderSize = CGSize(
            width: UIScreen.main.bounds.width,
            height: UIScreen.main.bounds.height
        )

        guard
            let documentDirectory = FileManager.default.urls(
                for: .documentDirectory,
                in: .userDomainMask
            ).first
        else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var date = dateFormatter.string(from: Date())
        let notNeededCharacters: Set<Character> = [",", ":", " "]
        date.removeAll(where: { notNeededCharacters.contains($0) })
        let dateString = String(date)

        let randomNumber = Int.random(in: 0 ..< 1000)
        let randomNumberString = String(randomNumber)
        let saveURL = documentDirectory.appendingPathComponent("mergeVideo-\(dateString)-\(randomNumberString).mp4")

        // rotation fix
        let assetVideoTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video).last!
        let compositionVideoTrack = mixComposition.tracks(withMediaType: AVMediaType.video).last!
        compositionVideoTrack.preferredTransform = assetVideoTrack.preferredTransform
        self.savedVideoURL = saveURL
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = saveURL
        assetExport.shouldOptimizeForNetworkUse = true

        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
            case AVAssetExportSession.Status.completed:

                // Uncomment this if u want to store your video in asset

                // let assetsLib = ALAssetsLibrary()
                // assetsLib.writeVideoAtPathToSavedPhotosAlbum(savePathUrl, completionBlock: nil)

                print("success")
                self.exportVideoToDevice(assetExport)
            case AVAssetExportSession.Status.failed:
                print("failed \(assetExport.error?.localizedDescription ?? "")")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error?.localizedDescription ?? "")")
            default:
                print("complete")
            }
        }
    }
}
