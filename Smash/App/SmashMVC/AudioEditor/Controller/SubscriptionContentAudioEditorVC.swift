import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    @objc func userBoughtSubscription() {
        configureCollectionViewLayout()
        self.AudioEditorControllerSetup()

        configureCollectionViewDataSource()
        createDummyData(forcePro: true)

        self.saveButton.removeFromSuperview()
        self.stopButton.removeFromSuperview()
        self.view.addSubview(self.saveButton)
        self.view.addSubview(self.stopButton)

        self.stopButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(134)
            make.height.equalTo(50.0)
            make.leading.equalToSuperview().inset(20)
        }

        self.saveButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(134)
            make.height.equalTo(50.0)
            make.trailing.equalToSuperview().inset(20)
        }
    }
}
