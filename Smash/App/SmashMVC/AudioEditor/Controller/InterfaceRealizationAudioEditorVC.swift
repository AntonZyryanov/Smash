import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    func playVideo(rate: Float? = nil) {
        let videoURL = self.recordedVideoURL!
        print("playVideo function working...")
        self.player = AVPlayer(url: videoURL)

        self.playerLayer = AVPlayerLayer(player: self.player)
        self.playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect // .resizeAspectFill
        print(self.videoView.frame)
        self.playerLayer?.frame = CGRect(origin: .zero, size: self.videoView.frame.size)
        self.videoView.layer.addSublayer(self.playerLayer!)
        self.player.volume = 0
        // size adjustments
        self.player.play()
        if let rate = rate {
            print("player speed changed player speed changed player speed changed player speed changed")
            self.player.rate = rate
        }
        print("video playing")
        view.addSubview(self.backButton)
        view.addSubview(self.shareButton)
    }

    @objc func updateProgressBar() {
        self.exportProgressBar.progress = self.exportSession!.progress
        if self.exportProgressBar.progress > 0.99 {
            self.timer.invalidate()
        }
    }

    func loadingIndicator() {
        self.saveButton.isEnabled = false
        //  let activityIndicatorView = UIActivityIndicatorView(style: .large)
        self.activityIndicatorView.color = ThemesColors.mainColor // UIColor(red: 162 / 256, green: 27 / 256, blue: 27 / 256, alpha: 1.0)
        // videoView.removeFromSuperview()
        self.view.addSubview(self.activityIndicatorView)
        self.activityIndicatorView.frame = self.view.frame
        self.activityIndicatorView.center = self.view.center

        self.activityIndicatorView.startAnimating()
        var extraTime: Double = 1.0
        /*      if self.savingTime < 5.0 {
             extraTime = 3.0
         }   */
        var ratio: Double = 1.1
        if self.chosenEffectIndicator == 0 {
            print("ratio changed to slow")
            ratio = 2
        }
        if self.chosenEffect == "none" {
            ratio = 0.0
            extraTime = Double(self.savingTime) / 5
        }
    }

    @objc func changeSoundEffectVolume(sender: UISlider) {
        self.backgroundSoundEffectPlayerNode.volume = sender.value
        self.soundEffectVolume = sender.value
    }
}
