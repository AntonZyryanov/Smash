import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    @objc func showDeniedAccessUI() {
        let alert = UIAlertController(
            title: "Allow access to your videos",
            message: "Please, let Smash to get access to all your photos to be able to proccess video with amazing audioeffects. Go to your settings and tap \"Photos\".",
            preferredStyle: .alert
        )

        let notNowAction = UIAlertAction(
            title: "Not Now",
            style: .cancel,
            handler: nil
        )
        alert.addAction(notNowAction)

        let openSettingsAction = UIAlertAction(
            title: "Open Settings",
            style: .default
        ) { [unowned self] _ in
            // Open app privacy settings
            gotoAppPrivacySettings()
        }
        alert.addAction(openSettingsAction)

        present(alert, animated: true, completion: nil)
    }

    func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
              UIApplication.shared.canOpenURL(url)
        else {
            assertionFailure("Not able to open App privacy settings")
            return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
