import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController: GADBannerViewDelegate {
    func bannerViewDidRecordClick(_ bannerView: GADBannerView) {
        print("User tapped on advisory banner")
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.userTappedOnBanner.rawValue)
    }
}

extension AudioEditorController: GADFullScreenContentDelegate {
    /// Tells the delegate that the ad failed to present full screen content.
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("Ad did fail to present full screen content.")
    }

    /// Tells the delegate that the ad presented full screen content.
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did present full screen content.")
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.interstitialShown.rawValue)
    }

    /// Tells the delegate that the ad dismissed full screen content.
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did dismiss full screen content.")

        if self.isVideoShared == true {
            self.present(self.activityViewController!, animated: true, completion: nil)

            self.isVideoShared = false
        }

        if self.whatToShowAfterVideoSaving == 3 {
            self.whatToShowAfterVideoSaving = 0
            guard let scene = self.view.window?.windowScene else {
                return
            }
            if #available(iOS 14.0, *) {
                SKStoreReviewController.requestReview(in: scene)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}
