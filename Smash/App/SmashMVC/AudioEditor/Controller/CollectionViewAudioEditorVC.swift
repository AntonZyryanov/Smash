import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

// COLLECTION VIEW EXTENSIONS

// MARK: - Collection View Delegate

extension AudioEditorController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StyleCell", for: indexPath) as! StyleCell

        // To set the selected cell background color here
        if self.currentSelected != nil, self.currentSelected == indexPath.row {
            cell.backgroundColor = UIColor(red: 255, green: 240, blue: 129, alpha: 1.0)
        } else {
            if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
                cell.backgroundColor = .init(red: 236 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0) // init(red: 142 / 255, green: 20 / 255, blue: 27 / 255, alpha: 1.0)
            } else {
                cell.backgroundColor = .init(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 1.0) // init(red: 142 / 255, green: 20 / 255, blue: 27 / 255, alpha: 1.0)
            }
        }

        return cell
    }

    func didSelectItemLogic(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let style = dataSource.itemIdentifier(for: indexPath) else { return }
        print(style)

        if self.videoIsExporting == true {
            print("Cell disabled ! Video is exporting!")
        } else {
            if self.backgroundSoundEffectChosen == true {
                print("Cell disabled ! Background sound effect loading!")

            } else {
                if indexPath.row == 0 {
                    self.stopButtonPressed()
                    let effectsPickerController = EffectsPickerController()
                    effectsPickerController.applyAudioEditorControllerSnapshotClosure = applySnapshot(styles:)
                    effectsPickerController.applyMainControllerSnapshotClosure = self.applyMainControllerSnapshotClosure
                    effectsPickerController.modalPresentationStyle = .pageSheet
                    effectsPickerController.modalTransitionStyle = .crossDissolve
                    effectsPickerController.isCalledFromMainController = false
                    present(effectsPickerController, animated: true)
                } else {
                    if Style.chosenStyles[indexPath.row].id > 14, Style.chosenStyles[indexPath.row].id < 40, Apphud.hasActiveSubscription() == false {
                        self.stopButtonPressed()
                        if SettingsController.isSubscribeButtonEnabled == true {
                            if Apphud.hasActiveSubscription() == false {
                                OnboardingPaywall.isCalledFromAudioVC = true
                                let paywallController = OnboardingPaywall()
                                self.navigationController?.pushViewController(paywallController, animated: true)
                            }
                        } else {
                            let alert = UIAlertController(
                                title: "Cant check your subscription status",
                                message: "Please check your internet connection and reload app",
                                preferredStyle: .alert
                            )
                            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                        }
                    } else {
                        self.chosenEffect = Style.chosenStyles[indexPath.row].styleName
                        Style.chosenStyles[indexPath.row].isSelected = true

                        applySnapshot(styles: Style.chosenStyles)

                        // highliting choosed effect
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateStyle = .long
                        dateFormatter.timeStyle = .short
                        var date = dateFormatter.string(from: Date())
                        let notNeededCharacters: Set<Character> = [",", ":", " "]
                        date.removeAll(where: { notNeededCharacters.contains($0) })
                        // print(date)
                        let randomNumber = Int.random(in: 0 ..< 10000)
                        print("last PlaySound function id:")
                        print(self.currentPlaySoundFunctionId)
                        self.currentPlaySoundFunctionId = date + String(randomNumber)
                        print("current PlaySound function id:")
                        print(self.currentPlaySoundFunctionId)
                        // TODO: Back
                        self.effectButtonPressed(index: Style.chosenStyles[indexPath.row].id, playSoundFunctionIdCheck: self.currentPlaySoundFunctionId)
                        self.chosenEffectIndicator = indexPath.row
                    }
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Request permission to access photo library
        if #available(iOS 14, *) {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { [unowned self] status in
                DispatchQueue.main.async { [unowned self] in
                    switch status {
                    case .authorized:
                        didSelectItemLogic(collectionView, didSelectItemAt: indexPath)
                        print("1")

                    case .limited:
                        //       showLimittedAccessUI()
                        showDeniedAccessUI()
                        print("2")
                    case .restricted:
                        //  showRestrictedAccessUI()
                        showDeniedAccessUI()
                        print("2")
                    case .denied:
                        //         showAccessDeniedUI()
                        showDeniedAccessUI()
                    case .notDetermined:
                        break

                    @unknown default:
                        break
                    }
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    self.didSelectItemLogic(collectionView, didSelectItemAt: indexPath)
                } else {
                    self.showDeniedAccessUI()
                }
            }
        }

        /*
         */
    }
}

// MARK: - Collection View Setup

extension AudioEditorController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return CGSize(width: 80.0, height: 80.0) // changed 22.06 was 80x80
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 1.0
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,

        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 1.0
    }
}

extension AudioEditorController {
    enum Section {
        case main
    }

    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        let groupSize = NSCollectionLayoutSize(widthDimension: .absolute(100), heightDimension: .absolute(100))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.scrollDirection = .horizontal
        let layout = UICollectionViewCompositionalLayout(section: section, configuration: config)
        return layout
    }

    func configureCollectionViewLayout() {
        // TODO: collectionView
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.createLayout())
        self.collectionView.delegate = self
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.collectionView.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        } else {
            self.collectionView.backgroundColor = .darkGray
        }
        // self.collectionView.backgroundColor = UIColor(red: 236 / 256, green: 235 / 256, blue: 235 / 256, alpha: 1.0)
        self.collectionView.register(StyleCell.self, forCellWithReuseIdentifier: StyleCell.reuseIdentifier)
    }

    func configureCollectionViewDataSource() {
        // TODO: dataSource
        self.dataSource = DataSource(collectionView: self.collectionView, cellProvider: { (collectionView, indexPath, style) -> StyleCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StyleCell.reuseIdentifier, for: indexPath) as! StyleCell
            cell.configure(with: style)
            cell.setupView()
            //  cell.backgroundColor = ThemesColors.mainColor
            if indexPath.row < 6 {
                cell.styleImageView.image = self.soundEffects[style.id].image
                if style.isSelected == true {
                    cell.isSelected = true
                    Style.chosenStyles[indexPath.row].isSelected = false
                }
            } else {
                if Apphud.hasActiveSubscription() {
                    cell.styleImageView.image = self.soundEffects[style.id].image
                    if style.isSelected == true {
                        cell.isSelected = true
                        Style.chosenStyles[indexPath.row].isSelected = false
                    }
                } else {
                    // TODO: back
                    cell.styleImageView.image = self.soundEffects[style.id].image
                    if style.isSelected == true {
                        cell.isSelected = true
                        Style.chosenStyles[indexPath.row].isSelected = false
                    }
                    // cell.styleImageView.image = UIImage(named: "locked")
                }
            }
            cell.highlightCell()
            cell.styleImageView.backgroundColor = ThemesColors.mainColor

            cell.isService = style.isService
            cell.isLocked = style.isLocked
            cell.nameLabel.text = style.styleName
            cell.isCellMarginIsBlack = UserDefaults.standard.bool(forKey: "isBlackThemeChoosed")
            cell.chooseBackgroundColor()
            cell.addNameLabel()
            cell.lockIfNeeded()
            return cell
        })
    }

    func createDummyData(forcePro: Bool = false) {
        var dummyStyles: [Style] = []
        print("Luke Skywalker")
        print(Apphud.hasActiveSubscription())
        for i in 0 ..< 40 {
            dummyStyles.append(Style(
                id: i,
                styleName: self.soundEffects[i].name // "Style \(i)"
            ))
        }
        Style.styles = dummyStyles

        if Apphud.hasActiveSubscription() == false, !forcePro {
            for i in 15 ..< 40 {
                Style.styles[i].isLocked = true
            }
        } else {
            for i in 15 ..< 40 {
                Style.styles[i].isLocked = false
            }
        }
        // add Effect picker cell
        Style.chosenStyles.removeAll()

        Style.chosenStyles.append(Style(id: 40, styleName: "plus", isSelected: false, isEnabled: false, isService: true))

        // saving chosen effects throughout sessions
        for index in UserDefaults.standard.stringArray(forKey: "chosenEffectsArray")! {
            Style.styles[Int(index)!].isEnabled = true
            Style.chosenStyles.append(Style.styles[Int(index)!])
        }

        //  Style.chosenStyles = dummyStyles
        self.applySnapshot(styles: Style.chosenStyles)
    }

    func fetchItems() {
        guard let url = URL(string: apiUrl) else { return }

        URLSession.shared.dataTask(with: url) { data, response, err in
            if let err = err {
                print(err.localizedDescription)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let fetchedItems = try JSONDecoder().decode([Style].self, from: data)
                        self.applySnapshot(styles: fetchedItems)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            } else {
                print("HTTPURLResponse code: \(response.statusCode)")
            }
        }.resume()
    }

    func applySnapshot(styles: [Style]) {
        // TODO: snapshot
        self.snapshot = DataSourceSnapshot()
        self.snapshot.appendSections([Section.main])
        self.snapshot.appendItems(styles)
        self.dataSource.apply(self.snapshot, animatingDifferences: false)
    }
}
