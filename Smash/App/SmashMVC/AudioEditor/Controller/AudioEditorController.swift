import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

class AudioEditorController: UIViewController {
    // MARK: Buttons declaration

    let stopButton = UIButton()
    let saveButton = UIButton()
    let backButton = UIButton()
    let shareButton = UIButton()

    let videoView = UIView()

    let lowPanel = UIView()

    // Holds File Location
    var recordedAudioURL: URL!
    var recordedVideoURL: URL!
    var audioFile: AVAudioFile!
    var soundEffectAudio: AVAudioFile!
    var audioEngine: AVAudioEngine!
    var audioPlayerNode: AVAudioPlayerNode!
    var backgroundSoundEffectPlayerNode: AVAudioPlayerNode!
    var stopTimer: Timer?

    var activityMonitor = UIActivityIndicatorView(style: .large)

    let soundEffects = SoundEffect.loadSoundEffects()

    // player

    var player = AVPlayer()
    var playerLayer: AVPlayerLayer?

    // saving time
    var savingTime: Float = 10.0
    // TODO: DataSource & DataSourceSnapshot typealias

    typealias DataSource = UICollectionViewDiffableDataSource<Section, Style>

    typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, Style>
    // COLLECTION VIEW  STUFF
    var collectionView: UICollectionView!
    let apiUrl = "https://jsonplaceholder.typicode.com/users"

    var dataSource: DataSource!
    var snapshot = DataSourceSnapshot()

    // MARK: - //- Audio effects

    var firstAudioAsset: AVAsset?
    var secondAudioAsset: AVAsset?

    // MARK: - //- Audio with video mixing

    var audioAsset: AVAsset?
    var videoAsset: AVAsset?

    //   MARK: - // - Chosen effect and chosen effect URL

    var chosenEffect: String = "none"
    var chosenEffectAudioURL: URL?
    var chosenEffectIndicator: Int = 1

    // MARK: if saved video have parallel transfer

    static var transferX: CGFloat = 0.0
    static var transferY: CGFloat = 0.0

    // Indicator
    let activityIndicatorView = UIActivityIndicatorView(style: .large)

    // Progress Bar
    var exportProgressBar = UIProgressView()
    var timer = Timer()
    var exportSession: AVAssetExportSession?

    var videoIsExporting: Bool = false
    var backgroundSoundEffectChosen: Bool = false

    var currentPlaySoundFunctionId: String = ""
    var playsoundFunctionIdChecker: String = ""

    var lastHighlitedEffect = IndexPath(row: 0, section: 0)

    // sound effect volume stuff
    let soundEffectVolumeSlider = UISlider()
    var soundEffectVolume: Float = 0.50
    var isSoundEffectSliderOn: Bool = false

    // cell highlighting stuff
    var previousSelected: IndexPath?
    var currentSelected: Int?

    // share video stuff

    var isVideoShared: Bool = false
    var activityViewController: UIActivityViewController?
    var savedVideoURL: URL?

    // Effects picker stuff
    var applyMainControllerSnapshotClosure: (([Style]) -> Void)?
    var applyBackToMainControllerSnapshotClosure: ((Bool) -> Void)?

    var interstitial: GADInterstitialAd?

    let banner: GADBannerView = {
        let banner = GADBannerView()
        banner.adUnitID = "ca-app-pub-1329450099126170/8233561396"
        banner.load(GADRequest())
        return banner
    }()

    var whatToShowAfterVideoSaving: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.userBoughtSubscription), name: Notification.Name("userBoughtSubscription"), object: nil)

        self.savingTime = self.calculateSavingTime(url: self.recordedAudioURL)

        configureCollectionViewLayout()
        self.AudioEditorControllerSetup()

        configureCollectionViewDataSource()
        createDummyData()
        setupAudio()

        // banner
        if Apphud.hasActiveSubscription() == false {
            self.banner.rootViewController = self
            self.banner.backgroundColor = UIColor(red: 85 / 255, green: 85 / 255, blue: 85 / 255, alpha: 0.3)
            self.banner.delegate = self
            view.addSubview(self.banner)

            self.banner.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.width.equalTo(320)
                make.height.equalTo(50)
                make.centerX.equalToSuperview()
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.playVideo()
            self.player.pause()
        }
    }

    override func viewDidAppear(_ animated: Bool) {}

    func calculateSavingTime(url: URL) -> Float {
        let asset = AVURLAsset(url: url, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        print(Float(audioDurationSeconds))
        return Float(audioDurationSeconds)
    }
}
