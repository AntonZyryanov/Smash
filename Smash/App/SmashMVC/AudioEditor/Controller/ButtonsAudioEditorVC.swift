import ApphudSDK
import AVFoundation
import AVKit
import GoogleMobileAds
import PhotosUI
import SnapKit
import StoreKit
import UIKit

extension AudioEditorController {
    @objc func goBack() {
        self.stopButtonPressed()
        self.applyBackToMainControllerSnapshotClosure!(false)
        self.navigationController?.popViewController(animated: true)
    }

    @objc func share() {
        self.isVideoShared = true
        self.saveVideo()
    }

    @objc func stopButtonPressed(choosedToSave: Bool = false) {
        self.stopAudioWithoutBlocking()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        var date = dateFormatter.string(from: Date())
        let notNeededCharacters: Set<Character> = [",", ":", " "]
        date.removeAll(where: { notNeededCharacters.contains($0) })
        // print(date)
        let randomNumber = Int.random(in: 0 ..< 10000)
        print("last PlaySound function id:")
        print(self.currentPlaySoundFunctionId)
        self.currentPlaySoundFunctionId = date + String(randomNumber)
        print("current PlaySound function id:")
        print(self.currentPlaySoundFunctionId)
        // configureUI(.notPlaying)
        self.player.pause()
        //  self.stopButton.isHidden = true
    }

    @objc func saveVideo() {
        self.videoIsExporting = true
        self.loadingIndicator()
        stopAudioWithoutBlocking()
        //  configureUI(.notPlaying)

        self.player.pause()
        self.configureUI(.playing)
        if self.chosenEffect == "none" {
            self.mixAudioWithVideo(videoUrl: self.recordedVideoURL, audioUrl: self.recordedAudioURL)
        } else {
            var extraTime: Double = 1.0
            if self.savingTime < 5.0 {
                extraTime = 3.0
            }
            var ratio: Double = 1.1
            if self.chosenEffectIndicator == 0 {
                print("ratio changed to slow")
                ratio = 2
            }
            if self.chosenEffectIndicator == 11 {
                ratio = 1.3
            }
            print(";)")
            print(extraTime)
            print(self.savingTime)
            print(";)")
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .short
            var date = dateFormatter.string(from: Date())
            let notNeededCharacters: Set<Character> = [",", ":", " "]
            date.removeAll(where: { notNeededCharacters.contains($0) })
            print(date)
            let randomNumber = Int.random(in: 0 ..< 10000)
            self.currentPlaySoundFunctionId = date + String(randomNumber)
            print("current PlaySound function id changed")
            print(self.currentPlaySoundFunctionId)
            self.effectButtonPressed(index: Style.chosenStyles[self.chosenEffectIndicator].id, choosedToSave: true, playSoundFunctionIdCheck: self.currentPlaySoundFunctionId)
        }
    }

    func effectButtonPressed(index: Int, choosedToSave: Bool = false, playSoundFunctionIdCheck: String) {
        if self.chosenEffect != "none" {
            self.player.pause()
        } // extra 2.08.21 in order to fix bugs
        self.stopAudioWithoutBlocking()
        //   self.configureUI(.notPlaying)
        configureUI(.playing)
        switch ButtonType(rawValue: index) {
        case .slow:
            applySoundEffect(rate: 0.5, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo(rate: 0.5)
        case .fast:
            applySoundEffect(rate: 1.5, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo(rate: 1.5)
        case .highPitch:
            applySoundEffect(pitch: 1000, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .forest:
            self.applySoundEffect(forest: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .distortion2:
            applySoundEffect(distortion2: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .sea:
            self.applySoundEffect(sea: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .reverb1:
            applySoundEffect(reverb: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .reverb2:
            applySoundEffect(reverb2: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .reverb3:
            applySoundEffect(reverb3: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .delay:
            applySoundEffect(delay: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .vader:
            applySoundEffect(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .echo:
            self.applySoundEffect(train: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .club:
            self.applySoundEffect(club: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()

        case .police:
            self.applySoundEffect(police: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .helicopter:
            self.applySoundEffect(helicopter: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .owl:
            self.applySoundEffect(owl: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .bonfire:
            self.applySoundEffect(bonfire: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .coffeeshop:
            self.applySoundEffect(coffeeshop: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .war:
            self.applySoundEffect(war: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .radiation:
            self.applySoundEffect(radiation: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .bus:
            self.applySoundEffect(bus: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .jungles:
            self.applySoundEffect(jungles: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .wasp:
            self.applySoundEffect(wasp: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .barbershop:
            self.applySoundEffect(barbershop: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .swamp:
            self.applySoundEffect(swamp: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .rain:
            self.applySoundEffect(rain: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .coop:
            self.applySoundEffect(coop: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .applause:
            self.applySoundEffect(applause: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .construction:
            self.applySoundEffect(construction: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .watch:
            self.applySoundEffect(watch: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .shower:
            self.applySoundEffect(shower: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .hairdryer:
            self.applySoundEffect(hairdryer: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .hurricane:
            self.applySoundEffect(hurricane: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .basketball:
            self.applySoundEffect(basketball: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .cave:
            self.applySoundEffect(cave: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .horse:
            self.applySoundEffect(horse: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .swings:
            self.applySoundEffect(swings: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .phone:
            self.applySoundEffect(phone: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .washingmachine:
            self.applySoundEffect(washingmachine: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .rockingchair:
            self.applySoundEffect(rockingchair: true, isBackgroundSoundEffect: true, choosedToSave: choosedToSave, playSoundFunctionIdCheck: playSoundFunctionIdCheck)
            self.playVideo()
        case .none:
            print("nothing")
        }
    }
}
