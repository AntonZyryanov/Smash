import ApphudSDK
import AVFoundation
import AVKit
import SnapKit
import UIKit

class EffectsPickerController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    let doneButton = UIButton()

    let imagePickerController = UIImagePickerController()
    var videoURL: NSURL?

    // picker of effects stuff

    // TODO: DataSource & DataSourceSnapshot typealias

    typealias DataSource = UICollectionViewDiffableDataSource<Section, Style>

    typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, Style>
    // COLLECTION VIEW  STUFF
    var collectionView: UICollectionView!
    let apiUrl = "https://jsonplaceholder.typicode.com/users"

    enum ButtonType: Int {
        case slow = 0, fast, highPitch, rain, wasp, reverb3, forest, distortion2, sea, echo, delay, vader, club, police, helicopter, owl, bonfire, coffeeshop, war, radiation,
             bus, jungles, reverb2, barbershop, swamp, reverb1, coop, applause, construction, watch, shower, hairdryer, hurricane, basketball, cave, horse, swings, phone,
             washingmachine,
             rockingchair
    }

    // TODO: dataSource & snapshot

    var dataSource: DataSource!
    var snapshot = DataSourceSnapshot()

    //
    let soundEffects = SoundEffect.loadSoundEffects()

    var isEffectsPanelShown: Bool = true

    var applyMainControllerSnapshotClosure: (([Style]) -> Void)?

    var applyAudioEditorControllerSnapshotClosure: (([Style]) -> Void)?

    var mainController: MainController?

    var isCalledFromMainController: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.themeChanged), name: Notification.Name("ThemeChanged"), object: nil)
        configureCollectionViewLayout()
        self.controllerInit()

        configureCollectionViewDataSource()
        createDummyData()

        // picker

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical // .horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.collectionView.setCollectionViewLayout(layout, animated: true)
        SmashAnalytics.analyticsEvent(event: SmashAnalytics.Events.effectsLibraryOpened.rawValue)
    }

    @objc func doneButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func themeChanged() {
        self.controllerInit()
    }
}
