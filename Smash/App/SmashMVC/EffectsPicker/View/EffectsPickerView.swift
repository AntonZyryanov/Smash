import ApphudSDK
import AVFoundation
import AVKit
import SnapKit
import UIKit

extension EffectsPickerController {
    func controllerInit() {
        self.navigationController?.navigationBar.isHidden = true
        self.title = "VIDEO LIBRARY"
        view.backgroundColor = UIColor.white

        view.addSubview(self.collectionView)

        self.doneButton.layer.cornerRadius = 25.0
        self.doneButton.layer.masksToBounds = true
        self.doneButton.setTitle(NSLocalizedString("Done", tableName: "EffectsPickerControllerLocalization", bundle: .main, value: "", comment: ""), for: .normal)
        self.doneButton.setTitleColor(ThemesColors.mainColor, for: .normal)
        self.doneButton.titleLabel?.textAlignment = .left
        self.doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        self.doneButton.addTarget(self, action: #selector(self.doneButtonPressed), for: .touchUpInside)

        self.view.addSubview(self.doneButton)
        self.collectionView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalToSuperview().inset(10)
            // make.top.equalTo(videoView.snp.bottom).inset(0)
            make.bottom.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(60)
        }
        self.doneButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(10)
            make.height.equalTo(50.0)
            make.width.equalTo(100.0)
            // make.centerX.equalToSuperview()
            make.trailing.equalToSuperview().inset(10)
        }
    }
}
