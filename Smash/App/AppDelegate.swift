import Amplitude
import ApphudSDK
import AppTrackingTransparency
import CoreData
import FacebookCore
import Firebase
import FirebaseRemoteConfig
import GoogleMobileAds
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    let settings = Settings.shared
    var router: AppRouter!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Apphud.start(apiKey: "app_tkj4KFb3gcAHt2QFjqLRczXFBbcWNZ")
        PurchasesProcessings.loadProducts()
        SmashAnalytics.facebookStart(application, launchOptions: launchOptions)
        SmashAnalytics.analyticsSetup()
        ThemesColors.setup()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration // .testDeviceIdentifiers = ["c326d0ecbc2ee2acf446d89f14a1df3e"]

        UINavigationBar.appearance().tintColor = Colors.mainColor
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        let container = NSPersistentCloudKitContainer(name: "Smash")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = self.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        let app = options[.sourceApplication] as? String

        return true
    }
}

extension AppDelegate {
    static let shared = UIApplication.shared.delegate as! AppDelegate
}
