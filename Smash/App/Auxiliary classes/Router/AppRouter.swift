import ApphudSDK
import FirebaseRemoteConfig
import Foundation
import UIKit

final class AppRouter {
    let window: UIWindow
    let settings: Settings

    init(window: UIWindow, settings: Settings) {
        self.window = window
        self.settings = settings
    }

    func startRouter() {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings

        self.fetchAndActivate(remoteConfig: remoteConfig, key: "isThereIsSale")
        self.fetchAndActivate(remoteConfig: remoteConfig, key: "isThereIsUpdate")
        self.fetchAndActivate(remoteConfig: remoteConfig, key: "updateURL")

        if UserDefaults.standard.bool(forKey: "isOnBoardingPassed") == true {
            if UserDefaults.standard.bool(forKey: "isThereIsSale") == false { // common situation
                //  self.showMainFlow()
                if Apphud.hasActiveSubscription() == false {
                    self.showPaywall()
                } else {
                    self.showMainFlow()
                }

            } else { // sale
                self.window.rootViewController = OnboardingViewController()
            }
        } else { // first time in app
            self.window.rootViewController = OnboardingViewController()
        }
    }

    func fetchAndActivate(remoteConfig: RemoteConfig, key: String) {
        remoteConfig.fetchAndActivate { status, error in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                if status != .error {
                    if let isSaleIsGoing =
                        remoteConfig[key].stringValue
                    {
                        UserDefaults.standard.setValue(Bool(isSaleIsGoing), forKey: key)
                    }
                }
            }
        }
    }

    func showPaywall() {
        self.window.rootViewController = OnboardingPaywall()
    }

    func showMainFlow() {
        let viewController = MainController()
        let navigation = UINavigationController(rootViewController: viewController)
        window.rootViewController = navigation
    }

    func showViewController(vc: UIViewController) {
        self.window.rootViewController?.show(vc, sender: self)
    }
}
