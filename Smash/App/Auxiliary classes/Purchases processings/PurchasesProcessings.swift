import ApphudSDK
import Foundation
import StoreKit

class PurchasesProcessings {
    static var paywall: ApphudPaywall?
    static var products: [ApphudProduct]?

    static func loadProducts() {
        Apphud.getPaywalls { paywalls, error in
            if error == nil {
                // retrieve current paywall with identifier
                self.paywall = paywalls?.first(where: { $0.identifier == "subscriptionsPaywall" })
                // retrieve the products [ApphudProduct] from current paywall
                self.products = self.paywall?.products
                SettingsController.isSubscribeButtonEnabled = true
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name("productsLoaded"), object: nil)
            }
        }
    }
}
