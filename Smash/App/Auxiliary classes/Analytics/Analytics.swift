import Amplitude
import FacebookCore
import Firebase
import Foundation

class SmashAnalytics {
    static let amplitude = Amplitude()

    enum Events: String {
        case firstOnBoardingPassed = "first_onboarding_passed"
        case secondOnBoardingPassed = "second_onboarding_passed"
        case thirdOnBoardingPassed = "third_onboarding_passed"
        case paywallPassed = "paywall_shown"
        case subscribeButtonPressed = "subscribe_button_pressed"
        case userTappedOnBanner = "user_tapped_on_banner"
        case interstitialShown = "interstitial_shown"
        case mainVCSettingsButtonPressed = "settings_button_pressed"
        case mainVCGalleryButtonPressed = "gallery_button_pressed"
        case mainVCPhotoshootButtonPressed = "photoshoot_button_pressed"
        case effectsLibraryOpened = "effects_library_opened"
        case saveButtonPressed = "save_button_pressed"
        case yearSubscription = "year_subscription_button_pressed"
        case monthSubscription = "month_subscription_button_pressed"
    }

    static func analyticsSetup() {
        FirebaseApp.configure()
        SmashAnalytics.amplitude.initializeApiKey("14f830b716cd79a32289120ae901bf0c")
    }

    static func analyticsEvent(event: String) {
        SmashAnalytics.amplitude.logEvent(event)
        Analytics.logEvent(event, parameters: nil)
        FacebookCore.AppEvents.logEvent(AppEvents.Name(rawValue: event))
    }

    static func analyticsEventWithProperties(event: String, properties: [String: Any]) {
        SmashAnalytics.amplitude.logEvent(event, withEventProperties: properties)
        Analytics.logEvent(event, parameters: properties)
    }

    static func facebookStart(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        FacebookCore.ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        FacebookCore.Settings.setDataProcessingOptions(["LDU"], country: 0, state: 0)
        FacebookCore.Settings.isAutoLogAppEventsEnabled = true
        FacebookCore.AppEvents.activateApp()
    }
}
