import UIKit

extension UIImage {
    static func localImage(_ name: String, template: Bool = false) -> UIImage {
        var image = UIImage(named: name)!
        if template {
            image = image.withRenderingMode(.alwaysTemplate)
        }
        return image
    }
}

final class Colors: NSObject {
    static let mainColor = ThemesColors
        .mainColor

    static let gradientCompanionColor = UIColor(red: 92.0 / 255.0, green: 81.0 / 255.0, blue: 212.0 / 255.0, alpha: 1.0)

    static let containerBackgroundColor2 = UIColor(red: 5.0 / 255.0, green: 12.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)

    static let containerBackgroundColor1 = UIColor(red: 87.0 / 255.0, green: 105.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)

    static let progressColor = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 0.5)
}
