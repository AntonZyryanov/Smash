import Foundation
import UIKit

class ThemesColors {
    static var mainColor = UIColor.systemBlue
    static var secondaryColor = UIColor.label
    static var whiteColor = UIColor.white

    static func setup() {
        if UserDefaults.standard.bool(forKey: "isBlackThemeChoosed") == false {
            self.secondaryColor = UIColor.black
        } else {
            self.secondaryColor = UIColor.label
        }
    }
}
