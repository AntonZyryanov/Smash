import Foundation
import KeychainAccess

final class Settings {
    static let shared = Settings()

    enum Keys: String {
        case onboardingPassed
        case apiToken
    }

    let keychain = Keychain()
    let defaults = UserDefaults.standard

    init() {
        self.onboardingPassed = self.defaults.bool(forKey: Keys.onboardingPassed.rawValue)
        self.token = try? self.keychain.get(Keys.apiToken.rawValue)
    }

    var onboardingPassed: Bool {
        didSet {
            self.defaults.set(self.onboardingPassed, forKey: Keys.onboardingPassed.rawValue)
        }
    }

    var token: String? {
        didSet {
            if let token = token {
                try? self.keychain.set(token, key: Keys.apiToken.rawValue)
            }
        }
    }
}
