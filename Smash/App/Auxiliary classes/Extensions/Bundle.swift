import Foundation
import UIKit

public extension Bundle {
    func languageBundle(code: String) -> Bundle? {
        self.localizations.first {
            $0.hasPrefix(code)
        }.flatMap {
            self.path(forResource: $0, ofType: "lproj")
        }.flatMap {
            Bundle(path: $0)
        }
    }

    func languageBundle(locale: Locale) -> Bundle? {
        switch (locale.languageCode, locale.scriptCode) {
        case let (.some(languageCode), .some(scriptCode)):
            return self.languageBundle(code: "\(languageCode)-\(scriptCode)")
        case let (.some(languageCode), .none):
            return self.languageBundle(code: languageCode)
        case (.none, .some), (.none, .none):
            return nil
        }
    }

    func loadImage(withName name: String) -> UIImage? {
        self.path(forResource: name, ofType: nil).flatMap {
            UIImage(contentsOfFile: $0)
        }
    }
}
